<?php

use App\Core\Routers\Web\RouterPool;
use Laminas\Di\Di;

define('APP_ROOT', __DIR__ . '/..');
require_once APP_ROOT . '/vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

$requestUri = $_SERVER['REQUEST_URI'] ?? null;

$di = new Di();
$dic = new \App\Core\Models\DIC\WebDIC($di);
$dic->assemble();
$router = $di->get(RouterPool::class);
$router->goToPage($requestUri);




