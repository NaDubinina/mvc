<?php

use App\Core\Routers\Console\CliRouter;
use Laminas\Di\Di;

define('APP_ROOT', __DIR__ . '/..');
require_once APP_ROOT . '/vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

//import --resource=api.itbook.store --keyword=mariadb
//send --name=kisaIgrisa --email=ndubinina@lachestry.com
//export --resource=books --type=csv
$arg['command'] = $argv[1] ?? 'export';


$arg = array_merge($arg, $argv ?? null);

$di = new Di();
$router = $di->get(CliRouter::class);
$router->execute($arg);