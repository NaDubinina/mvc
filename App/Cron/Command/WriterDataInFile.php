<?php

namespace App\Cron\Command;

use App\Book\Models\DatabaseInformationHandler\HandlerDataFromTables;
use App\Core\Models\DIC\CliDIC;

class WriterDataInFile implements ConsoleCommandInterface
{
    protected CliDIC $dic;
    protected HandlerDataFromTables $handlerData;
    private $writer;

    public function __construct(CliDIC $dic, HandlerDataFromTables $handlerData)
    {
        $this->handlerData = $handlerData;
        $this->dic = $dic;
        $this->dic->assemble();
        $this->writer = $this->dic->instanceManager->getClassFromAlias($this->dic::WRITER);
    }
    public function execute(array $arguments)
    {
        $this->writeDataInFile($arguments['resource']);
    }

    protected function writeDataInFile(string $resourceMethod)
    {
        print $this->handlerData->getFileName($resourceMethod);
        $writer = $this->dic->di->get(
            $this->writer,
            ['fileName' => $this->handlerData->getFileName($resourceMethod)]
        );

        $data = $this->handlerData->{$resourceMethod}();
        $writer->writeLinesFile($data);
    }
}
