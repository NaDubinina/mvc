<?php

namespace App\Cron\Command;

use App\Core\Models\DIC\CliDIC;
use App\User\Models\Services\Mail\SenderMail;

class SenderLetterToTheUser implements ConsoleCommandInterface
{
    protected CliDIC $dic;
    protected SenderMail $senderMail;

    public function __construct(CliDIC $dic, SenderMail $senderMail)
    {
        $this->senderMail = $senderMail;
        $this->dic = $dic;
    }

    public function execute(array $arguments)
    {
        $name = $arguments['name'];
        $email = $arguments['email'];
        $this->senderMail->sendMail($name, $email);
    }
}
