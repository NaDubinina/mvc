<?php

namespace App\Cron\Command;

use Laminas\Di\Di;

class ImporterDataApi implements ConsoleCommandInterface
{
    protected Di $di;

    public function __construct(Di $di)
    {
        $this->di = $di;
    }

    public function execute(?array $arguments)
    {
        $classResource = $arguments['resource']['class'];
        $importMethod = $arguments['resource']['method'];
        print $classResource;
        $this->importDataFromApi($classResource, $importMethod, $arguments['keyword']);
    }

    public function importDataFromApi($classResourceApi, $importMethod, $keywords)
    {
           ($this->di->get($classResourceApi))->{$importMethod}($keywords);
    }
}
