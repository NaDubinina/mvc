<?php

namespace App\Cron\Command;

class WriterInfoAboutCommands implements ConsoleCommandInterface
{
    private const DESCRIPTION = 'description';
    private const PARAMETERS = 'parameters';
    private $commandsInformation = [
        'info'   => [
            self::DESCRIPTION => 'Показывает информацию о всех доступных коммандах',
        ],
        'export' => [
            self::DESCRIPTION => 'Экспортирует информацию в выбранный тип файла. Обязательные параметры:',
            self::PARAMETERS => [
                '--type' => 'Принимает тип файла для записи. Доступные расширения файлов: csv.
            Пример использования: export --type=csv ',
                '--resource' => 'Принимает источник информации для записи. Доступные источники: books.
            books - Выдаёт информацию о всех известных копиях книг.
            Пример использования: export --resource=books '
            ]
        ]
    ];

    public function execute(?array $arguments)
    {
        $this->writeCommandsInformation();
    }

    protected function writeCommandsInformation()
    {
        foreach ($this->commandsInformation as $command => $comAnnotation) {
            print $command . ': ';
            if (isset($comAnnotation[self::DESCRIPTION])) {
                print $comAnnotation[self::DESCRIPTION] . PHP_EOL;
            }
            if (isset($comAnnotation[self::PARAMETERS])) {
                foreach ($comAnnotation[self::PARAMETERS] as $parameter => $paramAnnotation) {
                    print $parameter . ': ' . $paramAnnotation . PHP_EOL;
                }
            }
            print  PHP_EOL;
        }
    }
}
