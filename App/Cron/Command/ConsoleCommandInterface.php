<?php

namespace App\Cron\Command;

interface ConsoleCommandInterface
{
    public function execute(array $arguments);
}
