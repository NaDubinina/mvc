<?php

use App\Core\Models\Services\Writers\WriterCsvFileService;
use App\Core\Routers\Console\CliRouter;
use App\Cron\Command\ImporterDataApi;
use App\Cron\Command\SenderLetterToTheUser;
use App\Cron\Command\WriterDataInFile;
use App\Cron\Command\WriterInfoAboutCommands;
use App\ModuleSettingsAggregator;

return [
    ModuleSettingsAggregator::WEB_ROUTES_SETTINGS => [],
    ModuleSettingsAggregator::API_ROUTES_SETTINGS => [],
    ModuleSettingsAggregator::CLI_COMMAND_ROUTES_SETTINGS => [
        'info' => [
            CliRouter::CLASS_NAME => WriterInfoAboutCommands::class,
            CliRouter::PARAMETERS => [],
        ],
        'export' => [
            CliRouter::CLASS_NAME => WriterDataInFile::class,
            CliRouter::PARAMETERS => ['type', 'resource'],
        ],
        'import' => [
            CliRouter::CLASS_NAME => ImporterDataApi::class,
            CliRouter::PARAMETERS => ['resource', 'keyword'],
        ],
        'send' => [
            CliRouter::CLASS_NAME => SenderLetterToTheUser::class,
            CliRouter::PARAMETERS => ['name', 'email'],
        ],
    ],
    ModuleSettingsAggregator::CLI_PARAMS_ROUTES_SETTINGS => [
        'csv' => WriterCsvFileService::class,
    ]
];
