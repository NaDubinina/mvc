<?php

namespace App\Book\Blocks;

class UpdateBookCopyPage extends BaseFormBookCopy
{
    protected ?string $layout = 'update-book-copy-page.phtml';
    protected ?string $title = 'Изменение копии книги';
}
