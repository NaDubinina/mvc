<?php

namespace App\Book\Blocks;

use App\Bookcase\Models\SqlQueries\TableBookcaseQueries;
use App\Core\Blocks\Pages\BaseLayoutForm;

class BaseFormBookCopy extends BaseLayoutForm
{
    protected $methods = [
        'list of possible locations' => [
            'class' => TableBookcaseQueries::class,
            'method' => 'getLocation'
        ],
    ];

    private $bookCopyId = null;

    public function getBookCopyId(): int
    {
        return $this->bookCopyId;
    }

    public function setBookCopyId($bookCopyId): self
    {
        $this->bookCopyId = $bookCopyId;
        return $this;
    }

    public function renderListLocation()
    {
        $this->renderDataList('list of possible locations');
    }
}
