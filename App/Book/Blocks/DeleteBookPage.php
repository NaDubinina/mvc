<?php

namespace App\Book\Blocks;

class DeleteBookPage extends BaseFormBook
{
    protected ?string $layout = 'delete-book-page.phtml';
    protected ?string $title = 'Удаление книги';
}
