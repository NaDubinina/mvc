<?php

namespace App\Book\Blocks;

class UpdateBookPage extends BaseFormBook
{
    protected ?string $layout = 'update-book-page.phtml';
    protected ?string $title = 'Изменение книги';
}
