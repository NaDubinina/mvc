<?php

namespace App\Book\Blocks;

use App\Core\Blocks\BlockFactory;
use App\Core\Blocks\HeadBlock;
use App\Core\Blocks\Pages\BaseLayoutPage;
use App\Core\Models\Session;
use App\Core\Models\SqlQueries\AbstractTableQueries;
use App\User\Models\SqlQueries\TableUserQueries;
use Laminas\Di\Di;

class MyBooksPage extends BaseLayoutPage
{
    protected ?string $layout = 'my-books-page.phtml';
    protected ?string $title =  'Мои книги';
    protected ?string $styles = '/assets/css/pages-styles/books-page-styles.css';
    protected AbstractTableQueries $resource;

    public function __construct(
        Di $di,
        TableUserQueries $resource,
        BlockFactory $blockFactory,
        HeadBlock $headBlock,
        TableUserQueries $tableUserQueries
    ) {
        $this->resource = $tableUserQueries;
        parent::__construct($di, $resource, $blockFactory, $headBlock);
    }

    public function renderListMyBooks()
    {
        ($this->blockFactory->createBlock($this->listBlock))
            ->setData($this->resource->getListMyBooks(Session::getClientId()))
            ->setPageLink('/book-copy')
            ->render();
    }
}
