<?php

namespace App\Book\Blocks;

class DeleteBookCopyPage extends BaseFormBookCopy
{
    protected ?string $layout = 'delete-book-copy-page.phtml';
    protected ?string $title = 'Удаление копии книги';
}
