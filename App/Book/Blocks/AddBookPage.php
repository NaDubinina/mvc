<?php

namespace App\Book\Blocks;

class AddBookPage extends BaseFormBook
{
    protected ?string $layout = 'add-book-page.phtml';
    protected ?string $title = 'Добавление книги';
}
