<?php

namespace App\Book\Blocks;

use App\Core\Blocks\Pages\BaseLayoutPage;

class BooksPage extends BaseLayoutPage
{
    protected ?string $layout = 'books-page.phtml';
    protected ?string $title =  'Книги';
    protected ?string $styles = '/assets/css/pages-styles/books-page-styles.css';
}
