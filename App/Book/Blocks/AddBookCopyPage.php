<?php

namespace App\Book\Blocks;

class AddBookCopyPage extends BaseFormBookCopy
{
    protected ?string $layout = 'add-book-copy-page.phtml';
    protected ?string $title = 'Добавление копии книги';
    private $bookId = null;

    public function getBookId()
    {
        return $this->bookId;
    }

    public function setBookId($bookId): self
    {
        $this->bookId = $bookId;
        return  $this;
    }
}
