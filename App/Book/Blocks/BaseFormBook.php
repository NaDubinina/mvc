<?php

namespace App\Book\Blocks;

use App\Category\Models\SqlQueries\TableCategoryQueries;
use App\Core\Blocks\Pages\BaseLayoutForm;
use App\Country\Models\SqlQueries\TableCountryQueries;
use App\Publisher\Models\SqlQueries\TablePublisherQueries;

class BaseFormBook extends BaseLayoutForm
{
    protected $methods = [
        'list of possible categories' => [
            'class' => TableCategoryQueries::class,
            'method' => 'getNameCategories'
        ],
        'list of possible countries' => [
            'class' => TableCountryQueries::class,
            'method' => 'getCountries'
        ],
        'list of possible publishers' => [
            'class' => TablePublisherQueries::class,
            'method' => 'getNamePublishers'
        ],
    ];
    private $bookId = null;

    public function getBookId(): int
    {
        return $this->bookId;
    }

    public function setBookId($bookId): self
    {
        $this->bookId = $bookId;
        return  $this;
    }

    public function renderCategoryList()
    {
        $this->renderDataList('list of possible categories');
    }

    public function renderListCountry()
    {
        $this->renderDataList('list of possible countries');
    }

    public function renderListPublisher()
    {
        $this->renderDataList('list of possible publishers');
    }
}
