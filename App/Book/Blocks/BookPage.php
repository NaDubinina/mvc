<?php

namespace App\Book\Blocks;

use App\Book\Models\Entity\Book;
use App\Book\Models\SqlQueries\TableBookCopyQueries;
use App\Category\Models\SqlQueries\TableCategoryQueries;
use App\Core\Blocks\AbstractBlock;
use App\Core\Blocks\Pages\BaseLayoutPage;

class BookPage extends BaseLayoutPage
{
    protected ?string $layout = 'book-page.phtml';
    protected ?string $title =  'Книга';
    protected ?string $styles = '/assets/css/pages-styles/book-page-styles.css';
    protected ?Book $book = null;
    protected $abstractBlock = 'Core\\Blocks\\AbstractBlock';
    private $id = null;

    protected $methods = [
        'all copies of the book' => [
            'class' => TableBookCopyQueries::class,
            'method' => 'getBooksCopy',
            'entity' => 'book',
            'linkCard' => 'book',
            'getFieldEntity' => 'getId',
        ],
    ];

    public function getBookId(): int
    {
        return $this->id;
    }

    public function setBookId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getCategoryListBlock(): AbstractBlock
    {
        $block = $this->blockFactory->createBlock($this->abstractBlock);
        $resource = $this->di->newInstance(TableCategoryQueries::class);
        $dataBlock = $resource->getBookCategories($this->book->getCategoryId());
        $block->setData($dataBlock);
        $block->setBlockLink('book-categories-list.phtml');

        $block->setPageLink('/category')
            ->render();
        return $block;
    }

    public function renderLinkCountry()
    {
        ($this->blockFactory->createBlock($this->linkBlock))
            ->setData($this->resource->getTableRecord($this->book->getCountryId(), 'country'))
            ->setPageLink('/country?id=' . $this->book->getCountryId())
            ->render();
    }

    public function renderLinkPublisher()
    {
        ($this->blockFactory->createBlock($this->linkBlock))
            ->setData($this->resource->getTableRecord($this->book->getPublisherId(), 'publisher'))
            ->setPageLink('/publisher?id=' . $this->book->getPublisherId())
            ->render();
    }

    public function renderListBookCopy()
    {
        $this->renderSelectedDataList('all copies of the book');
    }
}
