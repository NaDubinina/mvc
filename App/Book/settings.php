<?php

use App\Book\Controllers\Api\BooksControllerApi;
use App\Book\Models\Api\ItBookStoreApi;
use App\Core\Models\Services\Writers\WriterCsvFileService;
use App\Core\Routers\Console\CliRouter;
use App\ModuleSettingsAggregator;

return [
    ModuleSettingsAggregator::WEB_ROUTES_SETTINGS => [],
    ModuleSettingsAggregator::API_ROUTES_SETTINGS => [
        '/api/books'  =>  BooksControllerApi::class,
    ],
    ModuleSettingsAggregator::CLI_COMMAND_ROUTES_SETTINGS => [],
    ModuleSettingsAggregator::CLI_PARAMS_ROUTES_SETTINGS => [
        'csv' => WriterCsvFileService::class,
        'books' => 'getBooksCopies',
        'api.itbook.store' => [
            CliRouter::CLASS_NAME => ItBookStoreApi::class,
            CliRouter::METHOD => 'importBookFromApi'
        ]
    ]
];
