<?php

namespace App\Book\Controllers\Api;

use App\Book\Models\SqlQueries\TableBookQueries;
use App\Core\Controllers\Api\AbstractControllerApi;
use Laminas\Di\Di;

class BooksControllerApi extends AbstractControllerApi
{
    private $tableName = 'book';

    public function __construct(TableBookQueries $resource, Di $di)
    {
        parent::__construct($di, $resource);
    }

    public function execute()
    {
        $this->handleRequest($this->tableName);
    }
}
