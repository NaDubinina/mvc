<?php

namespace App\Book\Controllers\AbstractControllers;

use App\Book\Models\SqlQueries\TableBookCopyQueries;
use App\Core\Blocks\BlockFactory;
use App\Core\Controllers\AbstractControllers\AbstractControllerForm;
use App\Core\Models\Services\Cache\BaseCacheService;
use Laminas\Di\Di;

class AbstractBookCopyEntityController extends AbstractControllerForm
{
    private const CACHE_NAME = 'BookCopyCache';

    public function render()
    {
        $this->blockFactory
            ->createBlock(static::PAGE)
            ->setBookCopyId($_GET['id'])
            ->render();
    }

    public function __construct(
        Di $di,
        BlockFactory $blockFactory,
        TableBookCopyQueries $resource,
        BaseCacheService $cacheService
    ) {
        $cacheService->getCacheClass()->setCacheName(self::CACHE_NAME);
        parent::__construct($di, $blockFactory, $resource, $cacheService);
    }
}
