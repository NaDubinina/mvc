<?php

namespace App\Book\Controllers\AbstractControllers;

use App\Book\Models\SqlQueries\TableBookQueries;
use App\Core\Blocks\BlockFactory;
use App\Core\Controllers\AbstractControllers\AbstractControllerForm;
use App\Core\Models\Services\Cache\BaseCacheService;
use Laminas\Di\Di;

class AbstractBookEntityController extends AbstractControllerForm
{
    private const CACHE_NAME = 'BookCache';

    public function render()
    {
        $this->blockFactory
            ->createBlock(static::PAGE)
            ->setBookId($_GET['id'])
            ->render();
    }

    public function __construct(
        Di $di,
        BlockFactory $blockFactory,
        TableBookQueries $resource,
        BaseCacheService $cacheService
    ) {
        $cacheService->getCacheClass()->setCacheName(self::CACHE_NAME);
        parent::__construct($di, $blockFactory, $resource, $cacheService);
    }
}
