<?php

namespace App\Book\Controllers\Web;

use App\Core\Controllers\AbstractControllers\AbstractControllerPage;

class BookControllerPage extends AbstractControllerPage
{
    protected $id = 4;
    public const PAGE = 'Book\\Blocks\\BookPage';

    public function render()
    {
        $this->blockFactory
            ->createBlock(self::PAGE)
            ->setBookId($this->id)
            ->render();
    }
}
