<?php

namespace App\Book\Controllers\Web;

use App\Book\Controllers\AbstractControllers\AbstractBookEntityController;
use App\Core\Models\Services\HandlerDataService;

class UpdateBookControllerPage extends AbstractBookEntityController
{
    public const PAGE = 'Book\\Blocks\\UpdateBookPage';

    public function submitForm()
    {
        $data = HandlerDataService::handlerSubmitData($_POST);
        $this->redirect('book?id=' . $this->resource->updateRecordInTable($data, $this->cacheService));
    }
}
