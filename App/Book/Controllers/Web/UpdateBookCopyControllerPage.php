<?php

namespace App\Book\Controllers\Web;

use App\Book\Controllers\AbstractControllers\AbstractBookCopyEntityController;
use App\Core\Models\Services\HandlerDataService;

class UpdateBookCopyControllerPage extends AbstractBookCopyEntityController
{
    public const PAGE = 'Book\\Blocks\\UpdateBookCopyPage';

    public function submitForm()
    {
        $data = HandlerDataService::handlerSubmitData($_POST);
        $this->redirect('book-copy?id=' . $this->resource->updateRecordInTable($data, $this->cacheService));
    }
}
