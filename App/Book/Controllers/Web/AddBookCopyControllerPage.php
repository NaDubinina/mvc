<?php

namespace App\Book\Controllers\Web;

use App\Book\Controllers\AbstractControllers\AbstractBookCopyEntityController;
use App\Core\Models\Services\HandlerDataService;

class AddBookCopyControllerPage extends AbstractBookCopyEntityController
{
    public const PAGE = 'Book\\Blocks\\AddBookCopyPage';

    public function render()
    {
        $this->blockFactory
            ->createBlock(self::PAGE)
            ->setBookId($_GET['book-id'])
            ->render();
    }

    public function submitForm()
    {
        $data = HandlerDataService::handlerSubmitData($_POST);
        $this->redirect('book?id=' . $this->resource->addRecordInTable($data, $this->cacheService));
    }
}
