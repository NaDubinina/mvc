<?php

namespace App\Book\Controllers\Web;

use App\Book\Controllers\AbstractControllers\AbstractBookCopyEntityController;
use App\Core\Models\Services\SecurityService;

class DeleteBookCopyControllerPage extends AbstractBookCopyEntityController
{
    public const PAGE = 'Book\\Blocks\\DeleteBookCopyPage';

    public function submitForm()
    {
        $prepareFormData = SecurityService::prepareDataForQuery($_POST, 'book_copy');
        $this->resource->deleteRecord($prepareFormData[':id']);
        $this->redirect('bookcase?id=' . $prepareFormData[':bookcase_id']);
    }
}
