<?php

namespace App\Book\Controllers\Web;

use App\Book\Controllers\AbstractControllers\AbstractBookEntityController;
use App\Core\Models\Services\HandlerDataService;

class AddBookControllerPage extends AbstractBookEntityController
{
    public const PAGE = 'Book\\Blocks\\AddBookPage';

    public function render()
    {
        $this->blockFactory
            ->createBlock(self::PAGE)
            ->render();
    }

    public function submitForm()
    {
        $data = HandlerDataService::handlerSubmitData($_POST);
        $this->redirect('book?id=' . $this->resource->addRecordInTable($data, $this->cacheService));
    }
}
