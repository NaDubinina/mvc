<?php

namespace App\Book\Controllers\Web;

use App\Book\Controllers\AbstractControllers\AbstractBookEntityController;
use App\Core\Models\Services\SecurityService;

class DeleteBookControllerPage extends AbstractBookEntityController
{
    public const PAGE = 'Book\\Blocks\\DeleteBookPage';

    public function submitForm()
    {
        $prepareFormData = SecurityService::prepareDataForQuery($_POST, 'book');
        $this->resource->deleteRecord($prepareFormData[':id']);
        $this->redirect('books');
    }
}
