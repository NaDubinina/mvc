<?php

namespace App\Book\Controllers\Web;

use App\Core\Controllers\AbstractControllers\AbstractControllerPage;

class BooksControllerPage extends AbstractControllerPage
{
    public const PAGE = 'Book\\Blocks\\BooksPage';
}
