<?php

namespace App\Book\Controllers\Web;

use App\Core\Controllers\AbstractControllers\AbstractControllerPage;

class MyBooksControllerPage extends AbstractControllerPage
{
    public const PAGE = 'Book\\Blocks\\MyBooksPage';
}
