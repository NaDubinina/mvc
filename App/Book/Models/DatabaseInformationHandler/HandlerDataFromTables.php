<?php

namespace App\Book\Models\DatabaseInformationHandler;

use App\Book\Models\SqlQueries\TableBookCopyQueries;

class HandlerDataFromTables
{
    private $fileName = [
        'getBooksCopies' => 'book_copy',
    ];
    private TableBookCopyQueries $resource;

    public function __construct(TableBookCopyQueries $resource)
    {
        $this->resource = $resource;
    }

    public function getBooksCopies(): array
    {
        $booksCopies = $this->resource->getTableAllRecords();
        return $this->handleBooksCopies($booksCopies);
    }

    public function getFileName($nameMethod): string
    {
        return $this->fileName[$nameMethod];
    }

    protected function handleBooksCopies($booksData): array
    {
        $booksCopies = [];
        $booksCopies['headers'] = $this->getHeadersBookCopy();
        foreach ($booksData as $book) {
            $bookCopy = $book->entityToArray();
            $book = $bookCopy['book'];
            unset($bookCopy['book']);
            $bookCopy = array_merge($book, $bookCopy);
            $booksCopies[] = $bookCopy;
        }

        return $booksCopies;
    }

    protected function getHeadersBookCopy()
    {
        $bookCopyFields = [];
        $allFields = TableBookCopyQueries::getFields();
        $bookCopyFields = array_merge($bookCopyFields, ['book_copy_id']);
        $bookCopyFields = array_merge($bookCopyFields, $allFields['book']);
        $bookCopyFields = array_merge($bookCopyFields, $allFields['book_copy']);

        return $bookCopyFields;
    }
}
