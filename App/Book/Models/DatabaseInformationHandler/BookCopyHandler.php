<?php

namespace App\Book\Models\DatabaseInformationHandler;

use App\Book\Models\Entity\Book;
use App\Book\Models\Entity\BookCopy;
use App\Core\Models\Entities\DatabaseInformationHandler\EntitiesHandler;

class BookCopyHandler extends EntitiesHandler
{
    public static function handleBookCopies($bookCopiesDB, ?Book $originBook, EntitiesHandler $instance): array
    {
        $bookCopies = [];

        $bookCopiesDB = $instance->arrayEntityInObjectEntity($bookCopiesDB);

        foreach ($bookCopiesDB as $bookCopyDB) {
            $bookCopies[] = self::handleBookCopy($bookCopyDB, $originBook, $instance);
        }

        return $bookCopies;
    }

    public static function handleBookCopy($bookCopyDB, ?Book $originBook, EntitiesHandler $instance): BookCopy
    {
        $bookCopyDB = $instance->replaceThreateningSymbols($bookCopyDB);
        return ($instance->di->newInstance(BookCopy::class))
            ->setId($bookCopyDB['id'])
            ->setBookId($bookCopyDB['book_id'])
            ->setBookcaseId($bookCopyDB['bookcase_id'])
            ->setCopyNumber($bookCopyDB['copy_number'])
            ->setBook($originBook);
    }
}
