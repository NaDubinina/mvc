<?php

namespace App\Book\Models\DatabaseInformationHandler;

use App\Book\Models\Entity\Book;
use App\Core\Models\Entities\DatabaseInformationHandler\EntitiesHandler;

class BookHandler extends EntitiesHandler
{
    public function handleBook($bookDB): Book
    {
        $bookDB = $this->replaceThreateningSymbols($bookDB);
        return ($this->di->newInstance(Book::class))
            ->setId($bookDB['id'])
            ->setName($bookDB['name'])
            ->setCountryId($bookDB['country_id'])
            ->setPublisherId($bookDB['publisher_id'])
            ->setCategoryId($bookDB['category_id'])
            ->setCreatedAt($bookDB['created_at'])
            ->setPrice($bookDB['price'])
            ->setAnnotation($bookDB['annotation'])
            ->setIsbn13($bookDB['isbn13'])
            ->setPages($bookDB['pages'])
            ->setImage($bookDB['image']);
    }
}
