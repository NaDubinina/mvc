<?php

namespace App\Book\Models\Entity;

use App\Core\Models\Entities\DatabaseInformation\AbstractEntity;

class Book extends AbstractEntity
{
    private $id = null;
    private $name = null;
    private $image = null;
    private $countryId = null;
    private $publisherId = null;
    private $createdAt = null;
    private $price = null;
    private $categoryId = null;
    private $annotation = null;
    private $pages = null;
    private $isbn13 = null;

    public function setPages($pages): self
    {
        $this->pages = $pages;
        return $this;
    }

    public function setIsbn13($isbn13): self
    {
        $this->isbn13 = $isbn13;
        return $this;
    }

    public function setAnnotation($annotation): self
    {
        $this->annotation = $annotation;
        return $this;
    }

    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }

    public function setCountryId($countryId): self
    {
        $this->countryId = $countryId;
        return $this;
    }

    public function setPublisherId($publisherId): self
    {
        $this->publisherId = $publisherId;
        return $this;
    }

    public function setCreatedAt($createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function setPrice($price): self
    {
        $this->price = $price;
        return $this;
    }

    public function setCategoryId($categoryId): self
    {
        $this->categoryId = $categoryId;
        return $this;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image ?: '/assets/img/book-cover.jpg';
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getCountryId()
    {
        return $this->countryId;
    }

    public function getPublisherId()
    {
        return $this->publisherId;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getCategoryId()
    {
        return $this->categoryId;
    }

    public function getPages()
    {
        return $this->pages;
    }

    public function getIsbn13()
    {
        return $this->isbn13;
    }

    public function getAnnotation()
    {
        return $this->annotation;
    }

    public function entityToArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'publisher_id' => $this->publisherId,
            'category_id' => $this->categoryId,
            'price' => $this->price,
            'country_id' => $this->countryId,
            'crated_at' => $this->createdAt,
            'image' => $this->image,
            'annotation' => $this->annotation,
            'isbn13' => $this->isbn13,
            'pages' => $this->pages,
        ];
    }
}
