<?php

namespace App\Book\Models\Api;

use App\Book\Models\SqlQueries\TableBookQueries;
use App\Core\Models\Environment;
use App\Core\Models\Services\Cache\BaseCacheService;
use App\Core\Models\Utils\HandlerJson;
use GuzzleHttp;
use Laminas\Di\Di;

class ItBookStoreApi
{
    public $fieldsApiAndFieldsTable = [
        'title'  => 'name',
        'isbn13' => 'isbn13',
        'image'  => 'image',
        'pages'  => 'pages',
        'year'   => 'created_at',
        'desc'   => 'annotation',
    ];
    public const ISBN = 'isbn13';
    public const IS_NEW_BOOK = 'isNewBook';
    private const CACHE_NAME = 'BookCache';
    private const BOOK = 'book';
    private const BOOK_LIST = 'books';
    private BaseCacheService $cacheService;
    private $client;
    private $apiUrlList;
    private $apiUrlEntity;
    private Di $di;
    private TableBookQueries $dbResource;

    public function __construct(Di $di, Environment $env, TableBookQueries $dbResource)
    {
        $this->di = $di;
        $this->cacheService = $this->di->get(BaseCacheService::class, ['cacheLocation' => self::CACHE_NAME]);
        $this->dbResource = $dbResource;
        $this->apiUrlList = $env->getApiUrlForList();
        $this->apiUrlEntity = $env->getApiUrlForEntity();
    }
    public function importBookFromApi($keywords)
    {
        /** @var GuzzleHttp\Client $client */
        $this->client = $this->di->get(GuzzleHttp\Client::class);

        $uri = $this->apiUrlList . $keywords;
        $response = $this->client->request('GET', $uri);
        $books = HandlerJson::fromJson($response->getBody());
        $this->updateDataApi($books);
    }

    protected function updateDataApi($newDataApi)
    {
        foreach ($newDataApi[self::BOOK_LIST] as $newBook) {
            $uri = $this->apiUrlEntity . $newBook[self::ISBN];
            $response = $this->client->request('GET', $uri);
            $newBookData = HandlerJson::fromJson($response->getBody());

            $result = $this->updateOldBook($newBookData);
            $book = $result[self::BOOK];

            if ($result[self::IS_NEW_BOOK]) {
                $this->dbResource->addRecordInTable($book, $this->cacheService);
            } elseif (isset($book)) {
                $this->dbResource->updateRecordInTable($book, $this->cacheService);
            }
        }
    }

    protected function updateOldBook($newDataBook): ?array
    {
        $isUpdate = false;
        $oldBook = $this->cacheService->getEntityByField(self::ISBN, $newDataBook[self::ISBN]);
        $isNewBook = is_null($oldBook);

        foreach ($this->fieldsApiAndFieldsTable as $apiField => $tableField) {
            if (isset($newDataBook[$apiField])) {
                if ($newDataBook[$apiField] !== $oldBook[$tableField]) {
                    $oldBook[$tableField] = $newDataBook[$apiField];
                    $isUpdate = true;
                }
            }
        }

        return [self::BOOK => (($isUpdate) ? $oldBook : null), self::IS_NEW_BOOK => $isNewBook];
    }
}
