<?php

namespace App\Book\Models\SqlQueries;

use App\Book\Models\DatabaseInformationHandler\BookCopyHandler;
use App\Book\Models\Entity\Book;
use App\Core\Database;
use App\Core\Models\SqlQueries\AbstractTableQueries;

class TableBookCopyQueries extends AbstractTableQueries
{
    public const TABLE_NAME = 'book_copy';

    public static function getFields(): array
    {
        return [
            'book_copy' => [
                'book_id',
                'copy_number',
                'bookcase_id'
            ],
            'book' => [
                'name',
                'publisher_id',
                'category_id',
                'price',
                'country_id',
                'created_at',
                'pages',
                'image',
                'isbn13',
                'annotation'
            ]
        ];
    }

    public function getBooksCopy($id): array
    {
        $connection = Database::getConnection();
        $query = 'SELECT * from book_copy where book_id=?';
        $query = $connection->prepare($query);
        $query->execute([$id]);
        $book = $this->getOriginBookForId($id);

        return BookCopyHandler::handleBookCopies($query->fetchAll(), $book, $this->entitiesHandler);
    }

    public function getBookBookcase($bookcaseId): array
    {
        $connection = Database::getConnection();
        $query = $connection->prepare('select * from book_copy where bookcase_id = ?');
        $query->execute([$bookcaseId]);
        $bookCopy = $query->fetchAll();
        $bookCopies = BookCopyHandler::handleBookCopies($bookCopy, null, $this->entitiesHandler);

        if ($bookCopies) {
            $query = $connection->prepare('select distinct book_id from book_copy where bookcase_id = ?');
            $query->execute([$bookcaseId]);
            $bookId = $query->fetchAll();

            foreach ($bookId as &$item) {
                $item = $item['book_id'];
            }

            $inList = str_repeat('?, ', count($bookId) - 1) . '?';
            $query = $connection->prepare("select * from book where id IN ($inList)");
            $query->execute($bookId);
            $queryData = $this->entitiesHandler->handleEntities($query->fetchAll(), 'book');

            foreach ($queryData as $book) {
                $books[$book->getId()] = $book;
            }

            foreach ($bookCopies as $bookCopy) {
                $bookId = $bookCopy->getBookId();
                $bookCopy->setBook($books[$bookId]);
            }
        }

        return $bookCopies;
    }

    public function addEntity($submittedFormData, $fieldsInStr, $valuesInStr): ?int
    {
        $connection = Database::getConnection();

        $query = "INSERT INTO book_copy ($fieldsInStr)  
                VALUES ($valuesInStr)";
        $query = $connection->prepare($query);
        $query->execute($submittedFormData);

        return $submittedFormData[':book_id'];
    }

    public function getTableRecord($id, $tableName = self::TABLE_NAME)
    {
        $connection = Database::getConnection();
        $query = "SELECT * from book_copy where id=?";
        $query = $connection->prepare($query);
        $query->execute([$id]);
        $bookCopy = $query->fetch();
        $book = $this->getOriginBookForId($bookCopy['book_id']);

        $entity = BookCopyHandler::handleBookCopy($bookCopy, $book, $this->entitiesHandler);

        return $entity;
    }

    public function getTableAllRecords($tableName = self::TABLE_NAME)
    {
        $connection = Database::getConnection();
        $query = "SELECT * from book_copy";
        $query = $connection->query($query);
        $queryData = $query->fetchAll();

        $booksCopies = [];

        foreach ($queryData as $bookCopy) {
            $book = $this->getOriginBookForId($bookCopy['book_id']);
            $booksCopies[] = BookCopyHandler::handleBookCopy($bookCopy, $book, $this->entitiesHandler);
        }

        return $booksCopies;
    }

    public function getOriginBookForId($id): Book
    {
        return ($this->di->newInstance(TableBookQueries::class))->getTableRecord($id);
    }
}
