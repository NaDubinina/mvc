<?php

namespace App\Book\Models\SqlQueries;

use App\Category\Models\SqlQueries\TableCategoryQueries;
use App\Core\Database;
use App\Core\Models\SqlQueries\AbstractTableQueries;

class TableBookQueries extends AbstractTableQueries
{
    public const TABLE_NAME = 'book';

    public static function getFields(): array
    {
        return [
            'book' => [
                'name',
                'publisher_id',
                'category_id',
                'price',
                'country_id',
                'created_at',
                'pages',
                'image',
                'isbn13',
                'annotation'
            ]
        ];
    }

    public function getCategoryBooks($categoryId): array
    {
        $connection = Database::getConnection();

        $categoryResource = $this->di->get(TableCategoryQueries::class);
        $categories = $categoryResource->getChildCategories($categoryId);
        $books = [];

        foreach ($categories as $category) {
            $categorId = $category->getId();
            $query = 'SELECT * from book where category_id=?';
            $query = $connection->prepare($query);
            $query->execute([$categorId]);

            $books = array_merge($books, $this->entitiesHandler->handleEntities($query->fetchAll(), 'book'));
        }

        return $books;
    }

    public function getCountryBooks($countryId): array
    {
        $connection = Database::getConnection();
        $query = 'select * from book where country_id = ?';
        $query = $connection->prepare($query);
        $query->execute([$countryId]);

        return $this->entitiesHandler->handleEntities($query->fetchAll(), 'book');
    }

    public function getPublisherBooks($publisherId): array
    {
        $connection = Database::getConnection();
        $query = 'SELECT * from book where publisher_id=?';
        $query = $connection->prepare($query);
        $query->execute([$publisherId]);

        return $this->entitiesHandler->handleEntities($query->fetchAll(), 'book');
    }

    public static function moveBooksParentCategory($idCategory, $parentIdCategory)
    {
        $connection = Database::getConnection();

        $fieldsCat[':id'] = $idCategory ?? null;
        $fieldsCat[':parent_id'] = $parentIdCategory ?? null;

        $query = 'update book 
            set `category_id` = ?
            where `category_id` = ?';
        $query = $connection->prepare($query);

        $query->execute([$fieldsCat[':parent_id'], $fieldsCat[':id']]);
    }
}
