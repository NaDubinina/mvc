<?php

namespace App\Publisher\Blocks;

use App\Book\Models\SqlQueries\TableBookQueries;
use App\Core\Blocks\Pages\BaseLayoutPage;

class PublisherPage extends BaseLayoutPage
{
    protected ?string $layout = 'publisher-page.phtml';
    protected ?string $title =  'Писатель';
    protected ?string $styles = '/assets/css/pages-styles/publisher-page-styles.css';
    protected $methods = [
        'all the publisher books' => [
            'class' => TableBookQueries::class,
            'method' => 'getPublisherBooks',
            'entity' => 'publisher',
            'linkCard' => 'book',
            'getFieldEntity' => 'getId',
        ],
    ];
    protected $id = null;

    public function getPublisherId(): int
    {
        return $this->id;
    }

    public function setPublisherId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function renderListPublisherBooks()
    {
        $this->renderSelectedDataList('all the publisher books');
    }
}
