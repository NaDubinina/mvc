<?php

namespace App\Publisher\Blocks;

use App\Core\Blocks\Pages\BaseLayoutPage;

class PublishersPage extends BaseLayoutPage
{
    protected ?string $layout = 'publishers-page.phtml';
    protected ?string $title =  'Авторы';
    protected ?string $styles = '/assets/css/pages-styles/publishers-page-styles.css';
}
