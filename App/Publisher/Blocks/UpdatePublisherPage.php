<?php

namespace App\Publisher\Blocks;

use App\Core\Blocks\Pages\BaseLayoutForm;

class UpdatePublisherPage extends BaseLayoutForm
{
    protected ?string $layout = 'update-publisher-page.phtml';
    protected ?string $title = 'Изменение писателя';
    private $publisherId = null;

    public function getPublisherId(): int
    {
        return $this->publisherId;
    }

    public function setPublisherId($publisherId): self
    {
        $this->publisherId = $publisherId;
        return $this;
    }
}
