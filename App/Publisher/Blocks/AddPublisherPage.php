<?php

namespace App\Publisher\Blocks;

use App\Core\Blocks\Pages\BaseLayoutForm;

class AddPublisherPage extends BaseLayoutForm
{
    protected ?string $layout = 'add-publisher-page.phtml';
    protected ?string $title = 'Добавление писателя';
}
