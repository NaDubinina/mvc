<?php

namespace App\Publisher\Models\SqlQueries;

use App\Core\Database;
use App\Core\Models\SqlQueries\AbstractTableQueries;

class TablePublisherQueries extends AbstractTableQueries
{
    public const TABLE_NAME = 'publisher';

    public static function getFields(): array
    {
        return [
            'publisher' => [
                'name',
                'surname',
                'date_of_birth',
                'date_of_death',
                'annotation'
            ]
        ];
    }

    public function getNamePublishers(): array
    {
        $connection = Database::getConnection();

        $query = 'select id, name, surname from publisher';
        $query = $connection->query($query);

        $publisherList = $query->fetchAll();
        foreach ($publisherList as &$publisher) {
            $publisher['name'] .= ' ' . $publisher['surname'];
        }

        return $publisherList;
    }
}
