<?php

namespace App\Publisher\Models\DatabaseInformationHandler;

use App\Core\Models\Entities\DatabaseInformationHandler\EntitiesHandler;
use App\Publisher\Models\Entity\Publisher;

class PublisherHandler extends EntitiesHandler
{
    public function handlePublisher($publisherDB): Publisher
    {
        $publisherDB = $this->replaceThreateningSymbols($publisherDB);
        return ($this->di->newInstance(Publisher::class))
            ->setId($publisherDB['id'])
            ->setName($publisherDB['name'])
            ->setSurname($publisherDB['surname'])
            ->setDateOfBirth($publisherDB['date_of_birth'])
            ->setDateOfDeath($publisherDB['date_of_death'])
            ->setAnnotation($publisherDB['annotation']);
    }
}
