<?php

namespace App\Publisher\Controllers\AbstractControllers;

use App\Core\Blocks\BlockFactory;
use App\Core\Controllers\AbstractControllers\AbstractControllerForm;
use App\Core\Models\Services\Cache\BaseCacheService;
use App\Publisher\Models\SqlQueries\TablePublisherQueries;
use Laminas\Di\Di;

class AbstractPublisherEntityController extends AbstractControllerForm
{
    private const CACHE_NAME = 'PublisherCache';

    public function __construct(
        Di $di,
        BlockFactory $blockFactory,
        TablePublisherQueries $resource,
        BaseCacheService $cacheService
    ) {
        $cacheService->getCacheClass()->setCacheName(self::CACHE_NAME);
        parent::__construct($di, $blockFactory, $resource, $cacheService);
    }


    public function render()
    {
        $this->blockFactory
            ->createBlock(static::PAGE)
            ->setPublisherId($_GET['id'])
            ->render();
    }
}
