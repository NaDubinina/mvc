<?php

namespace App\Publisher\Controllers\Web;

use App\Publisher\Controllers\AbstractControllers\AbstractPublisherEntityController;
use App\Core\Models\Services\SecurityService;

class DeletePublisherControllerPage extends AbstractPublisherEntityController
{
    public const PAGE = 'Publisher\\Blocks\\DeletePublisherPage';

    public function submitForm()
    {
        $prepareFormData = SecurityService::prepareDataForQuery($_POST, 'publisher');
        $this->resource->deleteRecord($prepareFormData[':id']);
        $this->redirect('publishers');
    }
}
