<?php

namespace App\Publisher\Controllers\Web;

use App\Core\Controllers\AbstractControllers\AbstractControllerPage;

class PublishersControllerPage extends AbstractControllerPage
{
    public const PAGE = 'Publisher\\Blocks\\PublishersPage';
}
