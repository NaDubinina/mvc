<?php

namespace App\Publisher\Controllers\Web;

use App\Core\Models\Services\HandlerDataService;
use App\Publisher\Controllers\AbstractControllers\AbstractPublisherEntityController;

class UpdatePublisherControllerPage extends AbstractPublisherEntityController
{
    public const PAGE = 'Publisher\\Blocks\\UpdatePublisherPage';

    public function submitForm()
    {
        $data = HandlerDataService::handlerSubmitData($_POST);
        $this->redirect('publisher?id=' . $this->resource->updateRecordInTable($data, $this->cacheService));
    }
}
