<?php

namespace App\Publisher\Controllers\Web;

use App\Core\Models\Services\HandlerDataService;
use App\Publisher\Controllers\AbstractControllers\AbstractPublisherEntityController;

class AddPublisherControllerPage extends AbstractPublisherEntityController
{
    public const PAGE = 'Publisher\\Blocks\\AddPublisherPage';

    public function render()
    {
        $this->blockFactory
            ->createBlock(self::PAGE)
            ->render();
    }

    public function submitForm()
    {
        $data = HandlerDataService::handlerSubmitData($_POST);
        $this->redirect('publisher?id=' . $this->resource->addRecordInTable($data, $this->cacheService));
    }
}
