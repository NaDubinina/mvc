<?php

namespace App\Publisher\Controllers\Web;

use App\Core\Controllers\AbstractControllers\AbstractControllerPage;

class PublisherControllerPage extends AbstractControllerPage
{
    protected $id = null;
    public const PAGE = 'Publisher\\Blocks\\PublisherPage';

    public function render()
    {
        $this->blockFactory
            ->createBlock(self::PAGE)
            ->setPublisherId($this->id)
            ->render();
    }
}
