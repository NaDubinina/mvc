<?php

namespace App\Publisher\Controllers\Api;

use App\Core\Controllers\Api\AbstractControllerApi;
use App\Publisher\Models\SqlQueries\TablePublisherQueries;
use Laminas\Di\Di;

class PublishersControllerApi extends AbstractControllerApi
{
    private $tableName = 'publisher';

    public function __construct(TablePublisherQueries $resource, Di $di)
    {
        parent::__construct($di, $resource);
    }

    public function execute()
    {
        $this->handleRequest($this->tableName);
    }
}
