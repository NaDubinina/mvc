<?php

use App\ModuleSettingsAggregator;
use App\Publisher\Controllers\Api\PublishersControllerApi;

return [
    ModuleSettingsAggregator::WEB_ROUTES_SETTINGS => [],
    ModuleSettingsAggregator::API_ROUTES_SETTINGS => [
        '/api/publishers'  => PublishersControllerApi::class
    ],
    ModuleSettingsAggregator::CLI_COMMAND_ROUTES_SETTINGS => [],
    ModuleSettingsAggregator::CLI_PARAMS_ROUTES_SETTINGS => []
];
