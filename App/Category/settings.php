<?php

use App\ModuleSettingsAggregator;

return [
    ModuleSettingsAggregator::WEB_ROUTES_SETTINGS => [],
    ModuleSettingsAggregator::API_ROUTES_SETTINGS => [],
    ModuleSettingsAggregator::CLI_COMMAND_ROUTES_SETTINGS => [],
    ModuleSettingsAggregator::CLI_PARAMS_ROUTES_SETTINGS => []
];
