<?php

namespace App\Category\Blocks;

use App\Category\Models\SqlQueries\TableCategoryQueries;
use App\Core\Blocks\Pages\BaseLayoutForm;

class BaseFormCategory extends BaseLayoutForm
{
    protected $methods = [
        'list of possible categories (including parents)' => [
            'class' => TableCategoryQueries::class,
            'method' => 'getFullNamesAllCategories'
        ],
    ];

    private $categoryId = null;

    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    public function setCategoryId($categoryId): self
    {
        $this->categoryId = $categoryId;
        return $this;
    }

    public function renderCategoryListWithParents()
    {
        $this->renderDataList('list of possible categories (including parents)');
    }
}
