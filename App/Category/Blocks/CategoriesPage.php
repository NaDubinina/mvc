<?php

namespace App\Category\Blocks;

use App\Core\Blocks\Pages\BaseLayoutPage;

class CategoriesPage extends BaseLayoutPage
{
    protected ?string $layout = 'categories-page.phtml';
    protected ?string $title =  'Категории';
    protected ?string $styles = '/assets/css/pages-styles/categories-page-styles.css';
}
