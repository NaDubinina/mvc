<?php

namespace App\Category\Blocks;

use App\Core\Blocks\Pages\BaseLayoutForm;

class DeleteCategoryPage extends BaseFormCategory
{
    protected ?string $layout = 'delete-category-page.phtml';
    protected ?string $title = 'Удаление категории';
}
