<?php

namespace App\Category\Blocks;

class UpdateCategoryPage extends BaseFormCategory
{
    protected ?string $layout = 'update-category-page.phtml';
    protected ?string $title = 'Изменение категории';
}
