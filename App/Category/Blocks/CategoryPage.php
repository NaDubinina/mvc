<?php

namespace App\Category\Blocks;

use App\Book\Models\SqlQueries\TableBookQueries;
use App\Core\Blocks\Pages\BaseLayoutPage;

class CategoryPage extends BaseLayoutPage
{
    protected ?string $layout = 'category-page.phtml';
    protected ?string $title =  'Категория';
    protected ?string $styles = '/assets/css/pages-styles/category-page-styles.css';
    protected $methods = [
        'all books related to the category' => [
            'class' => TableBookQueries::class,
            'method' => 'getCategoryBooks',
            'entity' => 'category',
            'linkCard' => 'book',
            'getFieldEntity' => 'getId',
        ],
    ];
    private $id = null;

    public function getCategoryId(): int
    {
        return $this->id;
    }

    public function setCategoryId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function renderListCategoryBooks()
    {
        $this->renderSelectedDataList('all books related to the category');
    }
}
