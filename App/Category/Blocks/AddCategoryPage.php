<?php

namespace App\Category\Blocks;

class AddCategoryPage extends BaseFormCategory
{
    protected ?string $layout = 'add-category-page.phtml';
    protected ?string $title = 'Добавление категории';
}
