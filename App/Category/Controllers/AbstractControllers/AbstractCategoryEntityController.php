<?php

namespace App\Category\Controllers\AbstractControllers;

use App\Category\Models\SqlQueries\TableCategoryQueries;
use App\Core\Blocks\BlockFactory;
use App\Core\Controllers\AbstractControllers\AbstractControllerForm;
use App\Core\Models\Services\Cache\BaseCacheService;
use Laminas\Di\Di;

class AbstractCategoryEntityController extends AbstractControllerForm
{
    private const CACHE_NAME = 'CategoryCache';

    public function __construct(
        Di $di,
        BlockFactory $blockFactory,
        TableCategoryQueries $resource,
        BaseCacheService $cacheService
    ) {
        $cacheService->getCacheClass()->setCacheName(self::CACHE_NAME);
        parent::__construct($di, $blockFactory, $resource, $cacheService);
    }

    public function render()
    {
        $this->blockFactory
            ->createBlock(static::PAGE)
            ->setCategoryId($_GET['id'])
            ->render();
    }
}
