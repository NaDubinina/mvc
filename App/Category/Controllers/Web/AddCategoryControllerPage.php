<?php

namespace App\Category\Controllers\Web;

use App\Category\Controllers\AbstractControllers\AbstractCategoryEntityController;
use App\Core\Models\Services\HandlerDataService;

class AddCategoryControllerPage extends AbstractCategoryEntityController
{
    public const PAGE = 'Category\\Blocks\\AddCategoryPage';

    public function render()
    {
        $this->blockFactory
            ->createBlock(self::PAGE)
            ->render();
    }

    public function submitForm()
    {
        $data = HandlerDataService::handlerSubmitData($_POST);
        $this->redirect('category?id=' . $this->resource->addRecordInTable($data, $this->cacheService));
    }
}
