<?php

namespace App\Category\Controllers\Web;

use App\Category\Controllers\AbstractControllers\AbstractCategoryEntityController;
use App\Core\Models\Services\HandlerDataService;

class UpdateCategoryControllerPage extends AbstractCategoryEntityController
{
    public const PAGE = 'Category\\Blocks\\UpdateCategoryPage';

    public function submitForm()
    {
        $data = HandlerDataService::handlerSubmitData($_POST);
        $this->redirect('category?id=' . $this->resource->updateRecordInTable($data, $this->cacheService));
    }
}
