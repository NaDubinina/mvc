<?php

namespace App\Category\Controllers\Web;

use App\Core\Controllers\AbstractControllers\AbstractControllerPage;

class CategoryControllerPage extends AbstractControllerPage
{
    protected $id = null;
    public const PAGE = 'Category\\Blocks\\CategoryPage';

    public function render()
    {
        $this->blockFactory
            ->createBlock(self::PAGE)
            ->setCategoryId($this->id)
            ->render();
    }
}
