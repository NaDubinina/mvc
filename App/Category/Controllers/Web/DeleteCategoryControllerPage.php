<?php

namespace App\Category\Controllers\Web;

use App\Category\Controllers\AbstractControllers\AbstractCategoryEntityController;
use App\Core\Models\Services\SecurityService;

class DeleteCategoryControllerPage extends AbstractCategoryEntityController
{
    public const PAGE = 'Category\\Blocks\\DeleteCategoryPage';

    public function submitForm()
    {
        $prepareFormData = SecurityService::prepareDataForQuery($_POST, 'category');
        $this->resource->deleteCategory($prepareFormData[':id'], $prepareFormData[':parent_id']);
        $this->redirect('categories');
    }
}
