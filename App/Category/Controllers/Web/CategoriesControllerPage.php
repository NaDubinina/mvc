<?php

namespace App\Category\Controllers\Web;

use App\Core\Controllers\AbstractControllers\AbstractControllerPage;

class CategoriesControllerPage extends AbstractControllerPage
{
    public const PAGE = 'Category\\Blocks\\CategoriesPage';
}
