<?php

namespace App\Category\Models\Entity;

use App\Core\Models\Entities\DatabaseInformation\AbstractEntity;

class Category extends AbstractEntity
{
    private $id = null;
    private $name = null;
    private $parentId = null;
    private $image = '/assets/img/category-cover.jpg';
    private $annotation = null;

    public function getAnnotation()
    {
        return $this->annotation;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getIdParent()
    {
        return $this->parentId;
    }

    public function setIdParent($parentId): self
    {
        $this->parentId = $parentId;
        return $this;
    }
    public function entityToArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'parent_id' => $this->parentId,
            'image' => $this->image,
            'annotation' => $this->annotation,
        ];
    }
}
