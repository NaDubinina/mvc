<?php

namespace App\Category\Models\SqlQueries;

use App\Book\Models\SqlQueries\TableBookQueries;
use App\Core\Database;
use App\Core\Models\SqlQueries\AbstractTableQueries;

class TableCategoryQueries extends AbstractTableQueries
{
    public const TABLE_NAME = 'category';

    public static function getFields(): array
    {
        return [
            'category' => [
                'name',
                'parent_id',
                'annotation'
            ]
        ];
    }

    public function getBookCategories($categoryId): array
    {
        $categories = [];
        $connection = Database::getConnection();
        $id = $categoryId;
        while ($id !== null) {
            $query = 'SELECT * from category where id=?';
            $query = $connection->prepare($query);
            $query->execute([$id]);
            $queryData = $query->fetch();
            $id = $queryData['parent_id'];
            $categories[] = $this->entitiesHandler->handleEntity(self::TABLE_NAME, $queryData);
        }

        return array_reverse($categories, false);
    }

    public function getChildCategories($categoryId): array
    {
        $connection = Database::getConnection();
        $categories = [];

        $categories[] = self::getTableRecord($categoryId);
        $i = 0;
        while (isset($categories[$i])) {
            $parentId = $categories[$i]->getId();

            $query = 'SELECT * from category where parent_id=?';
            $query = $connection->prepare($query);
            $query->execute([$parentId]);
            $queryData = $query->fetchAll();
            $childCategories = $this->entitiesHandler->handleEntities($queryData, 'category');

            $categories = array_merge($categories, $childCategories);
            $i++;
        }

        return $categories;
    }

    public function getFullNamesAllCategories(): array
    {
        $connection = Database::getConnection();

        $query = "SELECT * from category";
        $query = $connection->query($query);
        $queryData = $query->fetchAll();

        $categories = [];

        foreach ($queryData as $item) {
            $categories[$item['id']] = $item;
        }

        foreach ($categories as &$category) {
            if ($category['parent_id']) {
                $category['name'] = $categories[$category['parent_id']]['name'] . '/' . $category['name'];
            } else {
                $category['name'] = '/' . $category['name'];
            }
        }

        return $categories;
    }

    public function getFullNamesCategories($id): array
    {
        $connection = Database::getConnection();

        $notCategories = self::getChildCategories($id);
        $notInList = str_repeat('?, ', count($notCategories) - 1) . '?';
        foreach ($notCategories as &$item) {
            $item = $item->getId();
        }

        $query = "SELECT * from category where id NOT IN ($notInList)";
        $query = $connection->prepare($query);
        $query->execute($notCategories);
        $queryData = $query->fetchAll();

        foreach ($queryData as $item) {
            $categories[$item['id']] = $item;
        }

        foreach ($categories as &$category) {
            if ($category['parent_id']) {
                $category['name'] = $categories[$category['parent_id']]['name'] . '/' . $category['name'];
            } else {
                $category['name'] = '/' . $category['name'];
            }
        }

        return $categories;
    }

    public function deleteCategory($id, $parentId): bool
    {
        self::linkCategoryToParent($id, $parentId);
        TableBookQueries::moveBooksParentCategory($id, $parentId);
        self::deleteRecord($id);

        return true;
    }

    protected static function linkCategoryToParent($id, $parentId): bool
    {
        $connection = Database::getConnection();

        $fields[':id'] = $id ?? null;
        $fields[':parent_id'] = $parentId ?? null;

        $query = 'update category 
            set `parent_id` = ?
            where `parent_id` = ?';
        $query = $connection->prepare($query);

        $query->execute([$fields[':parent_id'], $fields[':id']]);
        return true;
    }

    public function getNameCategories()
    {
        $connection = Database::getConnection();

        $query = "SELECT id, name from category";
        $query = $connection->query($query);
        $queryData = $query->fetchAll();

        return $queryData;
    }
}
