<?php

namespace App\Category\Models\DatabaseInformationHandler;

use App\Category\Models\Entity\Category;
use App\Core\Models\Entities\DatabaseInformationHandler\EntitiesHandler;

class CategoryHandler extends EntitiesHandler
{
    public function handleCategory($categoryDB): Category
    {
        $categoryDB = $this->replaceThreateningSymbols($categoryDB);
        return ($this->di->newInstance(Category::class))
            ->setId($categoryDB['id'])
            ->setName($categoryDB['name'])
            ->setIdParent($categoryDB['parent_id']);
    }
}
