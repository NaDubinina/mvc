<?php

namespace App\User\Controllers\Web;

use App\Core\Blocks\BlockFactory;
use App\Core\Controllers\AbstractControllers\AbstractControllerPage;
use App\Core\Models\Session;
use App\User\Models\Services\AccountSecurityService;
use App\User\Models\SqlQueries\TableUserQueries;
use App\User\Models\UserDataValidator;

class UpdateUserInfoControllerPage extends AbstractControllerPage
{
    protected UserDataValidator $dataValidator;
    public const PAGE = 'User\\Blocks\\UpdateUserInfoPage';
    private TableUserQueries $resource;

    public function __construct(
        UserDataValidator $dataValidator,
        BlockFactory $blockFactory,
        TableUserQueries $resource
    ) {
        $this->dataValidator = $dataValidator;
        $this->resource = $resource;
        parent::__construct($blockFactory);
    }

    public function submitForm()
    {
        if (isset($_POST['logout'])) {
            Session::destroy();
            $this->redirect('');
        } elseif (isset($_POST['update'])) {
            $isError = $this->dataValidator->checkDataUpdate($_POST);
            $prepareFormData = AccountSecurityService::prepareDataForQuery($_POST, 'user', 'user_update');
            if ($isError == false) {
                $this->resource->updateUserInfo($prepareFormData);
                $this->redirect('user-account');
            }

            $this->render();
        }
    }
}
