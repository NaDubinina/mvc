<?php

namespace App\User\Controllers\Web;

use App\Core\Blocks\BlockFactory;
use App\Core\Controllers\AbstractControllers\AbstractControllerPage;
use App\User\Models\Services\Mail\SenderMail;

class UserControllerPage extends AbstractControllerPage
{
    protected SenderMail $senderMail;
    public const PAGE = 'User\\Blocks\\UserPage';

    public function __construct(BlockFactory $blockFactory, SenderMail $senderMail)
    {
        $this->senderMail = $senderMail;
        parent::__construct($blockFactory);
    }

    public function render()
    {
        $this->blockFactory
            ->createBlock(self::PAGE)
            ->setUserId($this->id)
            ->render();
    }

    public function submitForm()
    {
        $userData = $this->blockFactory
            ->createBlock(self::PAGE)
            ->setUserId($this->id)
            ->getEntity('user');

        $this->senderMail->sendMail(
            $userData->getName(),
            $userData->getEmail()
        );

        $this->redirect('');
    }
}
