<?php

namespace App\User\Controllers\Web;

use App\Core\Blocks\BlockFactory;
use App\Core\Controllers\AbstractControllers\AbstractControllerPage;
use App\User\Models\Services\Mail\SenderMail;

class UsersControllerPage extends AbstractControllerPage
{
    protected SenderMail $sender;
    public const PAGE = 'User\\Blocks\\UsersPage';

    public function __construct(BlockFactory $blockFactory, SenderMail $senderMail)
    {
        $this->sender = $senderMail;
        parent::__construct($blockFactory);
    }

    public function submitForm()
    {
        $this->sender->sendLetterAllUsers();
        $this->redirect('');
    }
}
