<?php

namespace App\User\Controllers\Web;

use App\Core\Blocks\BlockFactory;
use App\Core\Controllers\AbstractControllers\AbstractControllerPage;
use App\Core\Models\Session;
use App\User\Models\Services\AccountSecurityService;
use App\User\Models\SqlQueries\TableUserQueries;
use App\User\Models\UserDataValidator;

class UserRegistrationControllerPage extends AbstractControllerPage
{
    protected UserDataValidator $dataValidator;
    protected TableUserQueries $resource;
    public const PAGE = 'User\\Blocks\\RegistrationPage';

    public function __construct(
        BlockFactory $blockFactory,
        TableUserQueries $resource,
        UserDataValidator $dataValidator
    ) {
        $this->dataValidator = $dataValidator;
        $this->resource = $resource;
        parent::__construct($blockFactory);
    }

    public function submitForm()
    {
        $isError = $this->dataValidator->checkDataSignUp($_POST);
        $prepareFormData = AccountSecurityService::prepareDataForQuery($_POST, 'user', 'user_sign_up');

        if ($isError == false) {
            $clientId = $this->resource->signUp($prepareFormData);

            Session::setClientId($clientId);
            $this->redirect('user-account');
        } else {
            $this->render();
        }
    }
}
