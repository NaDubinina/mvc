<?php

namespace App\User\Controllers\Web;

use App\Core\Blocks\BlockFactory;
use App\Core\Controllers\AbstractControllers\AbstractControllerPage;
use App\Core\Models\Session;
use App\User\Models\Services\AuthService;

class UserAuthorizationControllerPage extends AbstractControllerPage
{
    protected AuthService $authService;
    public const PAGE = 'User\\Blocks\\LoginPage';

    public function __construct(BlockFactory $blockFactory, AuthService $authService)
    {
        $this->authService = $authService;
        parent::__construct($blockFactory);
    }

    public function submitForm()
    {
        $id = $this->authService->signIn($_POST);
        if (isset($id)) {
            Session::setClientId($id);
            $this->redirect('');
        } else {
            $this->render();
        }
    }
}
