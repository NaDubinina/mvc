<?php

namespace App\User\Controllers\Web;

use App\Core\Controllers\AbstractControllers\AbstractControllerPage;
use App\Core\Models\Session;

class UserAccountControllerPage extends AbstractControllerPage
{
    public const PAGE = 'User\\Blocks\\UserAccountPage';

    public function submitForm()
    {
        if (isset($_POST['logout'])) {
            Session::destroy();
            $this->redirect('');
        }
    }
}
