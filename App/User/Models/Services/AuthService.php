<?php

namespace App\User\Models\Services;

use App\Core\Models\Session;
use App\User\Models\SqlQueries\TableUserQueries;

class AuthService
{
    protected TableUserQueries $resource;

    public function __construct(TableUserQueries $resource)
    {
        $this->resource = $resource;
    }

    public function signIn($submittedFormData): ?int
    {
        $userData = $this->resource->getUserByLogin($submittedFormData['login']);

        if (password_verify($submittedFormData['password'], $userData['password'])) {
            return $userData['id'];
        } else {
            Session::setError('неверный логин или пароль');
        }

        return null;
    }
}
