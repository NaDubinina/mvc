<?php

declare(strict_types=1);

namespace App\User\Models\Services\Mail;

use App\Core\Blocks\Pages\BaseLayoutPage;
use App\Core\Exceptions\SendingMailException;
use App\Core\Models\Environment;
use App\User\Models\SqlQueries\TableUserQueries;
use Brevo;
use Exception;
use Laminas\Di\Di;

class SenderMail
{
    public const USER_NAME = 'name';
    public const USER_EMAIL = 'email';
    protected const LETTER_HEADER = 'TEST3';
    protected $template = '/views/mail/letter.phtml';
    protected $apiKey;
    protected $senderName;
    protected $senderMail;
    protected $api;
    protected Di $di;
    protected TableUserQueries $usersResource;

    public function __construct(Di $di, Environment $env, TableUserQueries $usersResource)
    {
        $this->di = $di;
        $this->apiKey = $env->getApiKey();
        $this->senderName = $env->getSenderName();
        $this->senderMail = $env->getSenderMail();
        $this->usersResource = $usersResource;

        $config = Brevo\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', $this->apiKey);

        $guzzle = $this->di->get(\GuzzleHttp\Client::class);
        $this->api = $this->di->get(
            Brevo\Client\Api\TransactionalEmailsApi::class,
            ['client' => $guzzle, 'config' => $config]
        );
    }

    public function sendMail(string $name, string $email)
    {
        $sendSmtpEmail = $this->di->get(
            Brevo\Client\Model\SendSmtpEmail::class,
            ['data' => [
                'subject' => self::LETTER_HEADER,
                'sender' => [self::USER_NAME => $this->senderName, self::USER_EMAIL => $this->senderMail],
                'replyTo' => [self::USER_NAME => $this->senderName, self::USER_EMAIL => $this->senderMail],
                'to' => [[self::USER_NAME => $name, self::USER_EMAIL => $email]],
                'htmlContent' =>  BaseLayoutPage::renderTemplate($this->template),
                ]
            ]
        );

        try {
            $this->api->sendTransacEmail($sendSmtpEmail);
        } catch (Exception $e) {
            throw new SendingMailException('Ошибка при отправке письма');
        }
    }

    public function sendLetterAllUsers()
    {
        $users = $this->usersResource->getUsersMail();
        foreach ($users as $userData) {
            $this->sendMail(
                $userData[SenderMail::USER_NAME],
                $userData[SenderMail::USER_EMAIL]
            );
        }
    }
}
