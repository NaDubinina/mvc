<?php

namespace App\User\Models\Services;

use App\Core\Models\Services\SecurityService;
use App\Core\Models\Session;
use App\User\Models\SqlQueries\TableUserQueries;

class AccountSecurityService extends SecurityService
{
    protected TableUserQueries $resource;

    public function __construct(TableUserQueries $resource)
    {
        $this->resource = $resource;
    }

    public function checkPasswordConsistency($submittedFormData): bool
    {
        if ($submittedFormData['repeat_password'] != $submittedFormData['password']) {
            Session::setError('Пароли не совпадают');
            return true;
        }

        return false;
    }

    public function checkUpdatePasswords(&$submittedFormData): bool
    {
        $clientId = Session::getClientId();
        $oldDataUser = $this->resource->getTableRecord($clientId);
        if (isset($submittedFormData['old_password'])) {
            if ($submittedFormData['old_password'] !== '') {
                if (!password_verify(strval($submittedFormData['old_password']), $oldDataUser->getPassword())) {
                    Session::setError('Вы ввели неверный текущий пароль');
                    return true;
                }
                if ($submittedFormData['password'] != $submittedFormData['repeat_password']) {
                    Session::setError('Пароли не совпадают');
                    return true;
                }
                if ($submittedFormData['password'] == '') {
                    Session::setError('Вы не ввели новый пароль');
                    return true;
                }
                if (password_verify($submittedFormData['password'], $oldDataUser->getPassword())) {
                    Session::setError('Новый и старый пароли совпадают');
                    return true;
                }
            } else {
                $submittedFormData['password'] = ($this->resource->getTableRecord($clientId))->getPassword();
            }
        }

        return false;
    }
}
