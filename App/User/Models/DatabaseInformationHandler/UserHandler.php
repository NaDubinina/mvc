<?php

namespace App\User\Models\DatabaseInformationHandler;

use App\Core\Models\Entities\DatabaseInformationHandler\EntitiesHandler;
use App\User\Models\Entity\User;

class UserHandler extends EntitiesHandler
{
    public function handleUser($userDB): User
    {
        $userDB = $this->replaceThreateningSymbols($userDB);

        return ($this->di->newInstance(User::class))
            ->setId($userDB['id'])
            ->setLogin($userDB['login'])
            ->setPassword($userDB['password'])
            ->setEmail($userDB['email']);
    }
}
