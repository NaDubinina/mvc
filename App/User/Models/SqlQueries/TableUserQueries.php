<?php

namespace App\User\Models\SqlQueries;

use App\Book\Models\DatabaseInformationHandler\BookCopyHandler;
use App\Core\Database;
use App\Core\Models\Session;
use App\Core\Models\SqlQueries\AbstractTableQueries;

class TableUserQueries extends AbstractTableQueries
{
    public const TABLE_NAME = 'user';

    public static function getFields(): array
    {
        return [
            'user' => ['login'],
            'user_update' => [
                'login',
                'email',
                'password',
                'repeat_password',
                'old_password'
            ],
            'user_sign_up' => [
                'login',
                'email',
                'password',
                'repeat_password'
            ],
        ];
    }

    public function getUserByLogin($login): array
    {
        $connection = Database::getConnection();

        $query = $connection->prepare('select * from user where login = ?');
        $query->execute([$login]);
        $queryData = $query->fetch();

        return $queryData;
    }

    public function signUp($submittedFormData): ?int
    {
        $connection = Database::getConnection();
        $fields = $submittedFormData;

        $query = 'INSERT INTO user (login, email, password)  
            VALUES (:login, :email, :password)';
        $query = $connection->prepare($query);
        $query->execute([$fields[':login'], $fields[':email'], $fields[':password']]);

        return $connection->lastInsertId();
    }

    public static function isSetLogin($login): bool
    {
        $connection = Database::getConnection();

        $query = $connection->prepare('select * from user where login = ?');
        $query->execute([$login]);
        $queryData = $query->fetch();
        if ($queryData === true) {
            return true;
        }
        return false;
    }

    public function updateUserInfo($submittedFormData): ?int
    {
        $connection = Database::getConnection();

        $fields = $submittedFormData;
        $fields[':id'] = Session::getClientId() ?? null;

        $update = 'update user 
            set `login` = :login, 
                `password` = :password
            where `id` = :id';
        $query = $connection->prepare($update);

        $query->execute([$fields[':login'], $fields[':password'], $fields[':id']]);

        return $fields[':id'];
    }

    public function getListMyBooks($userId): array
    {
        $connection = Database::getConnection();
        $query = $connection->prepare('select * from book_copy where user_id = ?');
        $query->execute([$userId]);
        $bookCopies = BookCopyHandler::handleBookCopies($query->fetchAll(), null, $this->entitiesHandler);

        if ($bookCopies) {
            $query = $connection->prepare('select distinct book_id from book_copy where user_id = ?');
            $query->execute([$userId]);
            $bookId = array_column($query->fetchAll(), 'book_id');

            $inList = implode(',', array_map('intval', $bookId));

            $query = $connection->query("select * from book where id IN ($inList)");
            $queryData = $this->entitiesHandler->handleEntities($query->fetchAll(), 'book');

            foreach ($queryData as $book) {
                $books[$book->getId()] = $book;
            }

            foreach ($bookCopies as $bookCopy) {
                $bookId = $bookCopy->getBookId();
                $bookCopy->setBook($books[$bookId]);
            }
        }

        return $bookCopies;
    }

    public function getUsersMail()
    {
        $connection = Database::getConnection();

        $query = $connection->query('select login as name, email from user');
        $queryData = $query->fetchAll();

        return $queryData;
    }
}
