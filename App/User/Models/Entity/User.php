<?php

namespace App\User\Models\Entity;

use App\Core\Models\Entities\DatabaseInformation\AbstractEntity;

class User extends AbstractEntity
{
    private $id = null;
    private $login = null;
    private $password = null;
    private $email = null;
    private $image = '/assets/img/publisher-cover.jpg';

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email): self
    {
        $this->email = $email;
        return $this;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getName()
    {
        return $this->login;
    }


    public function setLogin($login): self
    {
        $this->login = $login;
        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password): self
    {
        $this->password = $password;
        return $this;
    }

    public function entityToArray(): array
    {
        return [
            'id' => $this->id,
            'login' => $this->login,
            'password' => $this->password
        ];
    }
}
