<?php

namespace App\User\Models;

use App\Core\Models\Session;
use App\User\Models\Services\AccountSecurityService;
use App\User\Models\SqlQueries\TableUserQueries;
use Laminas\Di\Di;

class UserDataValidator
{
    protected Di $di;
    protected AccountSecurityService $securityService;
    protected TableUserQueries $resource;

    public function __construct(Di $di, AccountSecurityService $securityService, TableUserQueries $resource)
    {
        $this->di = $di;
        $this->resource = $resource;
        $this->securityService = $securityService;
    }

    public function checkDataSignUp($submittedFormData): bool
    {
        if (self::checkIsLogin($submittedFormData)) {
            return true;
        } else {
            return $this->securityService->checkPasswordConsistency($submittedFormData);
        }
    }

    public function checkDataUpdate($submittedFormData): bool
    {
        $oldDataUser = $this->resource->getTableRecord(Session::getClientId());
        $isNewLogin = false;
        if (($oldDataUser->getLogin() !== $submittedFormData['login'])) {
            $isNewLogin = true;
            if (UserDataValidator::checkIsLogin($submittedFormData)) {
                return true;
            };
        }

        if (!($isNewLogin) && ($submittedFormData['old_password'] == '')) {
            Session::setError('Вы не внесли изменения');
            return true;
        }

        return $this->securityService->checkUpdatePasswords($submittedFormData);
    }

    public static function checkIsLogin($submittedFormData): bool
    {
        if (TableUserQueries::isSetLogin($submittedFormData['login'])) {
            Session::setError('Пользоваеть с таким Логином уже зарегестрирован');
            return true;
        }
        return false;
    }
}
