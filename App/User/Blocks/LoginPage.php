<?php

namespace App\User\Blocks;

use App\Core\Blocks\Pages\BaseLayoutForm;

class LoginPage extends BaseLayoutForm
{
    protected ?string $layout = 'login-page.phtml';
    protected ?string $title = 'Авторизация';
}
