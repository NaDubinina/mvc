<?php

namespace App\User\Blocks;

use App\Core\Blocks\Pages\BaseLayoutPage;

class UsersPage extends BaseLayoutPage
{
    protected ?string $layout = 'main-page.phtml';
    protected ?string $title =  'Главная';
    protected ?string $styles = '/assets/css/pages-styles/main-page-styles.css';
}
