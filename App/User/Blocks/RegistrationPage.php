<?php

namespace App\User\Blocks;

use App\Core\Blocks\Pages\BaseLayoutForm;

class RegistrationPage extends BaseLayoutForm
{
    protected ?string $layout = 'registration-page.phtml';
    protected ?string $title = 'Регистрация';
}
