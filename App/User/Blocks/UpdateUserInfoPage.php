<?php

namespace App\User\Blocks;

use App\Core\Blocks\BlockFactory;
use App\Core\Blocks\HeadBlock;
use App\Core\Blocks\Pages\BaseLayoutForm;
use App\Core\Models\Session;
use App\User\Models\Entity\User;
use App\User\Models\SqlQueries\TableUserQueries;

class UpdateUserInfoPage extends BaseLayoutForm
{
    protected ?string $layout = 'update-user-info-page.phtml';
    protected ?string $title =  'Изменение личной информации';
    protected ?string $styles = '/assets/css/pages-styles/bookcase-page-styles.css';

    public function __construct(TableUserQueries $resource, BlockFactory $blockFactory, HeadBlock $headBlock)
    {
        parent::__construct($resource, $blockFactory, $headBlock);
    }

    public function getUserInformation(): User
    {
        return $this->resource->getTableRecord(Session::getClientId());
    }
}
