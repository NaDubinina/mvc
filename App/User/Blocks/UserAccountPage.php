<?php

namespace App\User\Blocks;

use App\Core\Blocks\BlockFactory;
use App\Core\Blocks\HeadBlock;
use App\Core\Blocks\Pages\BaseLayoutForm;
use App\Core\Models\Session;
use App\User\Models\Entity\User;
use App\User\Models\SqlQueries\TableUserQueries;

class UserAccountPage extends BaseLayoutForm
{
    protected ?string $layout = 'personal-account-page.phtml';
    protected ?string $title =  'Личный кабинет';
    protected ?string $styles = '/assets/css/pages-styles/bookcase-page-styles.css';

    public function __construct(
        TableUserQueries $resource,
        BlockFactory $blockFactory,
        HeadBlock $headBlock,
        TableUserQueries $tableUserQueries
    ) {
        $this->resource = $tableUserQueries;
        parent::__construct($resource, $blockFactory, $headBlock);
    }

    public function getUserInformation(): User
    {
        return $this->resource->getTableRecord(Session::getClientId());
    }
}
