<?php

namespace App\User\Blocks;

use App\Core\Blocks\Pages\BaseLayoutPage;

class UserPage extends BaseLayoutPage
{
    protected ?string $layout = 'user-page.phtml';
    protected ?string $title =  'Рассылка';
    protected ?string $styles = '/assets/css/pages-styles/books-page-styles.css';
    protected $id;

    public function getUserId(): int
    {
        return $this->id;
    }

    public function setUserId(int $id): self
    {
        $this->id = $id;
        return $this;
    }
}
