<?php

namespace App\Core\Models;

class Environment
{
    public const HOST = 'HOST';
    public const DATABASE_NAME = 'DB_NAME';
    public const PORT = 'PORT';
    public const DATABASE_USER = 'DB_USER';
    public const DATABASE_PASSWORD = 'DB_PASS';
    public const DATABASE_CHARSET = 'DB_CHARSET';
    public const REDIS_PASSWORD = 'REDIS_PASSWORD';
    public const CACHE_LOCATION = 'CACHE_LOCATION';
    public const API_LIST_ENTITY_URL = 'API_LIST_ENTITY_URL';
    public const API_ENTITY_URL = 'API_ENTITY_URL';
    public const API_KEY = 'API_KEY';
    public const SENDER_NAME = 'SENDER_NAME';
    public const SENDER_MAIL = 'SENDER_MAIL';
    public const EXPORT_FILE_TYPE = 'FILE_TYPE';
    protected $settings = null;

    public function __construct()
    {
        $this->settings = parse_ini_file(APP_ROOT . '/.env');
    }

    public function getDbHost()
    {
        return $this->settings[self::HOST] ?? '127.0.0.1';
    }

    public function getDbName()
    {
        return $this->settings[self::DATABASE_NAME] ?? null;
    }

    public function getDbPort()
    {
        return $this->settings[self::PORT] ?? '3306';
    }

    public function getDbUser()
    {
        return $this->settings[self::DATABASE_USER] ?? null;
    }

    public function getDbPassword()
    {
        return $this->settings[self::DATABASE_PASSWORD] ?? null;
    }

    public function getDbCharset()
    {
        return $this->settings[self::DATABASE_CHARSET] ?? 'utf8';
    }

    public function getRedisPassword()
    {
        return $this->settings[self::REDIS_PASSWORD] ?? null;
    }

    public function getCacheLocation()
    {
        return $this->settings[self::CACHE_LOCATION] ?? null;
    }

    public function getApiUrlForList()
    {
        return $this->settings[self::API_LIST_ENTITY_URL] ?? null;
    }

    public function getApiUrlForEntity()
    {
        return $this->settings[self::API_ENTITY_URL] ?? null;
    }

    public function getApiKey()
    {
        return $this->settings[self::API_KEY] ?? null;
    }

    public function getSenderName()
    {
        return $this->settings[self::SENDER_NAME] ?? null;
    }

    public function getSenderMail()
    {
        return $this->settings[self::SENDER_MAIL] ?? null;
    }

    public function getExportFileType()
    {
        return $this->settings[self::EXPORT_FILE_TYPE] ?? null;
    }

    public static function getFieldsStatically(): array
    {
        return parse_ini_file(APP_ROOT . '/.env');
    }
}
