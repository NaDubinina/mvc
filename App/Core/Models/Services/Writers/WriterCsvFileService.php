<?php

namespace App\Core\Models\Services\Writers;

class WriterCsvFileService implements WriterToFileServiceInterface
{
    protected $fileStream;
    protected $fileDirectory;

    public function __construct(string $fileName, string $fileDirectory = (APP_ROOT . '/var/output/'))
    {
        $file = $fileDirectory . $fileName . '_';
        $date = date('d_m_Y__H_i');
        $file .= $date;
        $this->fileStream = fopen($file, 'w');
        $this->fileDirectory = $fileDirectory;
    }

    public function __destruct()
    {
        fclose($this->fileStream);
    }

    public function writeLinesFile(array $dataToWrite)
    {
        foreach ($dataToWrite as $line) {
            fputcsv($this->fileStream, $line);
        }
    }
}
