<?php

namespace App\Core\Models\Services\Writers;

interface WriterToFileServiceInterface
{
    public function __construct(string $fileName, string $fileDirectory = (APP_ROOT . '/var/output/'));
    public function writeLinesFile(array $dataToWrite);
}
