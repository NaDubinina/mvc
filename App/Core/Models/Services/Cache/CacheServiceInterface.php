<?php

namespace App\Core\Models\Services\Cache;

interface CacheServiceInterface
{
    public function isCache(): bool;
    public function setCacheName(string $cacheName);
    public function setCache(array $data);
    public function getCache(): ?array;
}
