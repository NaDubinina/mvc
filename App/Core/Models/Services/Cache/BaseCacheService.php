<?php

namespace App\Core\Models\Services\Cache;

use App\Core\Models\DIC\WebDIC;
use App\Core\Models\Entities\DatabaseInformation\AbstractEntity;

class BaseCacheService
{
    public const VALUES = 'values';
    public const IS_CACHED_ALL = 'is_cached_all';
    public ?WebDIC $dic;
    private ?CacheServiceInterface $cacheService;

    public function __construct(WebDIC $dic)
    {
        $this->dic = $dic;
        $this->dic->assemble();
        $cacheLocation = $this->dic->instanceManager->getClassFromAlias($dic::CACHE_LOCATION);
        $cacheLocationConfig = $this->dic->instanceManager->getConfig($dic::CACHE_LOCATION);
        $this->cacheService = $this->dic->di->get($cacheLocation, $cacheLocationConfig['parameters']);
    }

    public function updateCacheEntity(array $data)
    {
        $cache = $this->cacheService->getCache();
        $entityPosition = $this->getPositionEntityInCache($data[AbstractEntity::ID]) ?? count($cache);

        $cache[$this::VALUES][$entityPosition] = $data;
        $cache[$this::IS_CACHED_ALL] = $cache[$this::IS_CACHED_ALL] ?? false;
        $this->updateCache($cache);
    }

    public function deleteCacheByEntity(int $id): bool
    {
        $cache = $this->cacheService->getCache();
        $positionEntity = $this->getPositionEntityInCache($id);
        if (is_null($positionEntity)) {
            return false;
        }
        array_splice($cache[$this::VALUES], $positionEntity, 1);
        $cache[$this::IS_CACHED_ALL] = false;
        $this->updateCache($cache);
        return true;
    }

    public function setCacheEntitiesById(array $data): bool
    {
        if ($this->getPositionEntityInCache($data[AbstractEntity::ID]) !== null) {
            return false;
        }
        $cache = $this->cacheService->getCache();
        if (!isset($cache[$this::VALUES])) {
            $cache[$this::VALUES] = [$data];
            $cache[$this::IS_CACHED_ALL] = false;
        } else {
            $cache[$this::VALUES][] = $data;
        }

        $this->cacheService->setCache($cache);
        return true;
    }

    public function isCacheEntitiesById($id): bool
    {
        if (is_null($this->getPositionEntityInCache($id))) {
            return false;
        }
        return true;
    }

    public function getCacheClass()
    {
        return $this->cacheService;
    }

    public function getEntityByField($fieldName, $fieldValue)
    {
        $cache = $this->cacheService->getCache();

        foreach ($cache['values'] as $item) {
            if (isset($item[$fieldName])) {
                if ($item[$fieldName] == $fieldValue) {
                    return $item;
                }
            }
        }
        return null;
    }

    protected function getPositionEntityInCache($id): ?int
    {
        $oldCache = $this->cacheService->getCache();
        if (isset($oldCache)) {
            foreach ($oldCache[$this::VALUES] as $key => $item) {
                if ($item[AbstractEntity::ID] == $id) {
                    return $key;
                }
            }
        }
        return  null;
    }

    protected function updateCache($newCache)
    {
        $this->cacheService->deleteCache();
        $this->cacheService->setCache($newCache);
    }
}
