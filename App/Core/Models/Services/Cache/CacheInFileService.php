<?php

namespace App\Core\Models\Services\Cache;

use App\Core\Models\Utils\HandlerCache;
use App\Core\Models\Utils\HandlerJson;

class CacheInFileService implements CacheServiceInterface
{
    private string $cacheFile;

    public function __construct($cacheLocation)
    {
        $this->cacheFile = APP_ROOT . '/var/cache/' . $cacheLocation;
    }

    public function setCacheName(string $cacheName)
    {
        $this->cacheFile = APP_ROOT . '/var/cache/' . $cacheName;
    }

    public function isCache(): bool
    {
        return file_exists($this->cacheFile);
    }

    public function setCache(array $data)
    {
        $cache = HandlerCache::handleCache($data, $this);
        if (isset($cache)) {
            file_put_contents(($this->cacheFile), $cache);
        }
    }

    public function getCache(): ?array
    {
        if (!$this->isCache()) {
            return null;
        }
        $cache = file_get_contents($this->cacheFile);
        return HandlerJson::fromJson($cache);
    }

    public function deleteCache()
    {
        unlink($this->cacheFile);
    }
}
