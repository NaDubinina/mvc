<?php

namespace App\Core\Models\Services\Cache;

use App\Core\Models\Environment;
use App\Core\Models\Utils\HandlerCache;
use App\Core\Models\Utils\HandlerJson;
use Predis;

class CacheInRedisService implements CacheServiceInterface
{
    private $redis;
    private $cacheKey;

    public function __construct($cacheLocation)
    {
        $this->redis = new Predis\Client();
        $settings = Environment::getFieldsStatically();
        $this->redis->auth($settings[Environment::REDIS_PASSWORD]);
        $this->cacheKey = $cacheLocation;
    }

    public function setCacheName(string $cacheName)
    {
        $this->cacheKey = $cacheName;
    }

    public function isCache(): bool
    {
        return $this->redis->exists($this->cacheKey);
    }

    public function setCache(?array $data)
    {
        $cache = HandlerCache::handleCache($data, $this);
        if (isset($cache)) {
            $this->redis->set($this->cacheKey, $cache);
        }
    }

    public function getCache(): ?array
    {
        if (!$this->isCache()) {
            return null;
        }
        $cache = $this->redis->get($this->cacheKey);
        return HandlerJson::fromJson($cache);
    }

    public function deleteCache()
    {
        $this->redis->del($this->cacheKey);
    }
}
