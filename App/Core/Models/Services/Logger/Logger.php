<?php

namespace App\Core\Models\Services\Logger;

class Logger
{
    protected const ERRORS_FILE = APP_ROOT . '/var/logs/errors.log';
    protected const WARNINGS_FILE = APP_ROOT . '/var/logs/warnings.log';
    protected const TIME_FORMAT = ' d.m.y H:i ';

    public function writeLogError($message)
    {
        file_put_contents(self::ERRORS_FILE, 'Error: ' . $message .
        ' Date:' . date(self::TIME_FORMAT) . PHP_EOL, FILE_APPEND);
    }

    public function writeLogWarning($message)
    {
        file_put_contents(self::WARNINGS_FILE, 'Warning: ' . $message . PHP_EOL .
        ' Date:' . date(self::TIME_FORMAT) . PHP_EOL, FILE_APPEND);
    }
}
