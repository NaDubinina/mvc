<?php

namespace App\Core\Models\Services;

class SecurityService
{
    public static function hashPassword($password): string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    public static function prepareDataForQuery($submittedFormData, $tableName, $subField = null): array
    {
        $fields = HandlerDataService::getFieldsByTableName($tableName, $subField);

        if ($submittedFormData !== null) {
            $fields = array_intersect($fields, array_keys($submittedFormData));
        }
        $prepareFormData = [];
        foreach ($fields as $field) {
            $prepareFieldName = ':' . $field;
            $prepareFormData[$prepareFieldName] = $submittedFormData[$field] ?? null;
            $prepareFormData[$prepareFieldName] = htmlspecialchars($prepareFormData[$prepareFieldName]);
        }

        if (isset($submittedFormData['id'])) {
            $prepareFormData[':id'] = $submittedFormData['id'];
        }
        if (isset($prepareFormData[':password'])) {
            $prepareFormData[':password'] = SecurityService::hashPassword($prepareFormData[':password']);
        }

        return $prepareFormData;
    }
}
