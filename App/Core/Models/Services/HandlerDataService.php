<?php

namespace App\Core\Models\Services;

use App\Core\Models\SqlQueries\AbstractTableQueries;
use App\ModuleSettingsAggregator;

class HandlerDataService
{
    public static function handleFieldsForAdd($fields, $tableName)
    {
        $fieldsInStr = '';
        $valuesInStr = '';

        $tableFields = self::getFieldsByTableName($tableName);

        foreach ($fields as $field) {
            if (in_array($field, $tableFields) && strlen($field) > 0) {
                $fieldsInStr .= $field . ', ';
                $valuesInStr .= ':' . $field . ', ';
            }
        }

        $fieldsInStr = substr($fieldsInStr, 0, -2);
        $valuesInStr = substr($valuesInStr, 0, -2);

        return [AbstractTableQueries::FIELDS => $fieldsInStr, AbstractTableQueries::VALUES => $valuesInStr];
    }

    public static function handleFieldsForUpdate($fields, $tableName)
    {
        $fieldsPlusValuesInStr = '';

        $tableFields = self::getFieldsByTableName($tableName);

        foreach ($fields as $field) {
            if (in_array($field, $tableFields)) {
                $fieldsPlusValuesInStr .= $field . ' = :' . $field . ', ';
            }
        }

        $fieldsPlusValuesInStr = substr($fieldsPlusValuesInStr, 0, -2);
        return $fieldsPlusValuesInStr;
    }

    public static function getResourceByTableName($entityName): string
    {
        $resourceName = self::handlerClassName($entityName);
        $dirName = ModuleSettingsAggregator::getModuleDir($entityName);
        $class = "\\App\\{$dirName}\\Models\\SqlQueries\\Table" . $resourceName . 'Queries';
        return $class;
    }

    public static function getFieldsByTableName($tableName, $fields = null): array
    {
        $fields = is_null($fields) ? $tableName : $fields;
        $resource = self::getResourceByTableName($tableName);
        $tableFields = $resource::getFields();
        return $tableFields[$fields];
    }

    public static function handlerClassName($entityName, $separator = '_'): string
    {
        $entitySplit = explode($separator, $entityName);
        $className = array_map('ucfirst', $entitySplit);
        return implode('', $className);
    }

    public static function handlerSubmitData($data): array
    {
        unset($data['token']);
        foreach ($data as $key => $values) {
            if (!strlen($values) > 0) {
                unset($data[$key]);
            }
        }

        return $data;
    }
}
