<?php

declare(strict_types=1);

namespace App\Core\Models\DIC;

use App\Core\Models\Environment;
use App\Core\Models\Services\Cache\CacheInFileService;
use App\Core\Models\Services\Cache\CacheInRedisService;
use App\Core\Models\Services\Cache\CacheServiceInterface;
use App\Core\Routers\Web\ApiRouter;
use App\Core\Routers\Web\Router;
use App\Core\Routers\Web\RouterPool;
use Laminas\Di\Di;
use Laminas\Di\InstanceManager;

abstract class AbstractDiC
{
    public const CACHE_LOCATION = 'caching_location';
    public InstanceManager $instanceManager;
    public Di $di;
    public Environment $env;

    public function __construct(Di $di)
    {
        $this->di = $di;
        $this->instanceManager = $di->instanceManager();

        /** @var Environment $this */
        $this->env = $this->di->get(Environment::class);
    }

    public function assemble()
    {
        $reflection = new \ReflectionClass($this);
        foreach ($reflection->getMethods(\ReflectionMethod::IS_PROTECTED) as $method) {
            if (strpos($method->getName(), 'assemble') === 0) {
                $method->setAccessible(true);
                $method->invoke($this);
            }
        }
    }

    protected function assembleRouters()
    {
        $this->instanceManager->setParameters(
            RouterPool::class,
            [
                'routers' => [
                    $this->di->get(ApiRouter::class),
                    $this->di->get(Router::class),
                ]
            ]
        );
    }

    protected function assembleCache()
    {
        $this->instanceManager->addAlias(
            self::CACHE_LOCATION,
            $this->env->getCacheLocation() === 'redis' ? CacheInRedisService::class : CacheInFileService::class,
            ['cacheLocation' => 't']
        );

        $this->instanceManager->addTypePreference(
            CacheServiceInterface::class,
            self::CACHE_LOCATION
        );
    }
}
