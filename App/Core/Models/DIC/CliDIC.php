<?php

namespace App\Core\Models\DIC;

use App\Core\Models\Services\Writers\WriterCsvFileService;
use App\Core\Models\Services\Writers\WriterToFileServiceInterface;

class CliDIC extends AbstractDiC
{
    public const WRITER = 'writer';
    private $writers = [
        'csv' => [
            'class' => WriterCsvFileService::class,
            'fileName' => 'BookExport']
    ];

    protected function assembleWriter()
    {
        print $this->env->getExportFileType();
        $type = $this->writers[$this->env->getExportFileType()];
        $this->instanceManager->addAlias(
            self::WRITER,
            $type['class'],
            [ 'fileName' => $type['fileName']]
        );

        $this->instanceManager->addTypePreference(
            WriterToFileServiceInterface::class,
            self::WRITER
        );
    }
}
