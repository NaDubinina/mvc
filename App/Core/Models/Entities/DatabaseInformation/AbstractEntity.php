<?php

namespace App\Core\Models\Entities\DatabaseInformation;

abstract class AbstractEntity
{
    abstract public function entityToArray(): array;
    public const ID = 'id';
}
