<?php

namespace App\Core\Models\Entities\DatabaseInformationHandler;

use Laminas\Di\Di;

class EntitiesHandler
{
    protected Di $di;
    protected HandlerFactory $handlerFactory;
    public function __construct(Di $di, HandlerFactory $handlerFactory)
    {
        $this->handlerFactory = $handlerFactory;
        $this->di = $di;
    }

    public function arrayEntityInObjectEntity($entities)
    {
        if (isset($entities['id'])) {
            $entity = $entities;
            $entities = [];
            $entities[] = $entity;
        }

        return$entities;
    }

    public function handleEntities($dataDB, $entityName): array
    {
        $entities = [];

        $handleClass = $this->handlerFactory->getHandler($this->convertSmallCharactersIntoLarge($entityName));

        $methodHandler = $this->convertEntityNameToMethod($entityName);
        $dataDB = $this->arrayEntityInObjectEntity($dataDB);

        foreach ($dataDB as $entityDB) {
            $entities[] = $handleClass->{$methodHandler}($entityDB);
        }

        return $entities;
    }

    public function handleEntity($entityName, $entityDB)
    {
        $handleClass = $this->handlerFactory->getHandler($this->convertSmallCharactersIntoLarge($entityName));
        $methodHandler = $this->convertEntityNameToMethod($entityName);
        $entity = $handleClass->{$methodHandler}($entityDB);
        return $entity;
    }

    protected function replaceThreateningSymbols($submittedDbData)
    {
        foreach ($submittedDbData as &$submittedDbField) {
            $submittedDbField = htmlspecialchars($submittedDbField);
        }

        return $submittedDbData;
    }

    protected function convertSmallCharactersIntoLarge($entityName): string
    {
        $entityName = explode('_', $entityName);
        return implode('', array_map('ucfirst', $entityName));
    }

    protected function convertEntityNameToMethod($entityName): string
    {
        $entityName = $this->convertSmallCharactersIntoLarge($entityName);
        $methodHandler = 'handle' . $entityName ;

        return $methodHandler;
    }
}
