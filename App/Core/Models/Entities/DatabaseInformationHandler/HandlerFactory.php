<?php

declare(strict_types=1);

namespace App\Core\Models\Entities\DatabaseInformationHandler;

use Laminas\Di\Di;

class HandlerFactory
{
    protected Di $di;

    public function __construct(Di $di)
    {
        $this->di = $di;
    }

    public function getHandler(string $entity): EntitiesHandler
    {
        $classHandler = "App\\{$entity}\\Models\\DatabaseInformationHandler\\{$entity}Handler";
        return $this->di->get($classHandler);
    }
}
