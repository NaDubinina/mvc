<?php

namespace App\Core\Models\Utils;

class HandlerJson
{
    public static function toJson(?array $data): ?string
    {
        if (isset($data)) {
            return json_encode($data);
        }
        return  null;
    }

    public static function fromJson(string $data): array
    {
        return json_decode($data, true);
    }
}
