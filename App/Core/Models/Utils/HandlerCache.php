<?php

namespace App\Core\Models\Utils;

use App\Core\Models\Services\Cache\BaseCacheService;
use App\Core\Models\Services\Cache\CacheServiceInterface;

class HandlerCache
{
    public static function handleCache($data, CacheServiceInterface $cacheService): ?string
    {
        $cache = $data;
        $oldCache = $cacheService->getCache();
        if (isset($oldCache)) {
            $cache[BaseCacheService::VALUES] = array_merge(
                $cache[BaseCacheService::VALUES],
                $oldCache[BaseCacheService::VALUES]
            );
            $cache[BaseCacheService::IS_CACHED_ALL] = $oldCache[BaseCacheService::IS_CACHED_ALL];
        }
        return HandlerJson::toJson($cache);
    }
}
