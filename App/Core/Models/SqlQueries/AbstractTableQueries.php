<?php

namespace App\Core\Models\SqlQueries;

use App\Core\Database;
use App\Core\Exceptions\NotFoundException;
use App\Core\Models\Entities\DatabaseInformation\AbstractEntity;
use App\Core\Models\Entities\DatabaseInformationHandler\EntitiesHandler;
use App\Core\Models\Services\Cache\BaseCacheService;
use App\Core\Models\Services\HandlerDataService;
use App\Core\Models\Services\SecurityService;
use Laminas\Di\Di;

class AbstractTableQueries
{
    public const TABLE_NAME = null;
    public const FIELDS = 'fields';
    public const VALUES = 'values';
    public EntitiesHandler $entitiesHandler;
    protected Di $di;

    public function __construct(Di $di, EntitiesHandler $entitiesHandler, HandlerDataService $handlerDataService)
    {
        $this->di = $di;
        $this->entitiesHandler = $entitiesHandler;
        $this->handlerDataService = $handlerDataService;
    }

    public function getTableRecord(?int $id, string $tableName = null)
    {
        $tableName = isset($tableName) ? $tableName : static::TABLE_NAME;
        $connection = Database::getConnection();
        $query = "SELECT * from $tableName where id=?";
        $query = $connection->prepare($query);
        $query->execute([$id]);

        $className = $this->handlerDataService->handlerClassName($tableName . '_');
        $dataQuery = $query->fetch();
        if (!$dataQuery) {
            http_response_code(404);
            throw new NotFoundException();
        }

        return $this->entitiesHandler->handleEntity($tableName, $dataQuery);
    }

    public function getTableAllRecords(string $tableName = null)
    {
        $tableName = isset($tableName) ? $tableName : static::TABLE_NAME;
        $connection = Database::getConnection();

        $query = "SELECT * from $tableName";
        $query = $connection->query($query);
        $queryData = $query->fetchAll();

        $entity = $this->entitiesHandler->handleEntities($queryData, $tableName);

        return $entity;
    }

    public function deleteRecord(int $id): bool
    {
        $tableName = static::TABLE_NAME;
        $connection = Database::getConnection();
        $fields[':id'] = $id ?? null;

        $query = "delete from $tableName where `id` = :id";
        $query = $connection->prepare($query);

        $query->execute($fields);

        return true;
    }

    public function addEntity($submittedFormData, $fieldsInStr, $valuesInStr): ?int
    {
        $connection = Database::getConnection();

        $table = static::TABLE_NAME;
        $query = "INSERT INTO $table ($fieldsInStr)
                VALUES ($valuesInStr)";
        $query = $connection->prepare($query);
        $query->execute($submittedFormData);

        return $connection->lastInsertId();
    }

    public function updateEntity($submittedFormData, $fields): ?int
    {
        $connection = Database::getConnection();

        $table = static::TABLE_NAME;

        $update = "update $table 
            set $fields
            where `id` = :id";
        $query = $connection->prepare($update);

        $query->execute($submittedFormData);


        return $submittedFormData[':id'];
    }

    public function deleteEntity($id, BaseCacheService $baseCacheService): bool
    {
        self::deleteRecord($id);
        return $baseCacheService->deleteCacheByEntity($id);
    }

    public function addRecordInTable($data, BaseCacheService $baseCacheService): ?int
    {
        $addEntity = self::handleDataByMethod('add');
        $fields = $this->handlerDataService->handleFieldsForAdd(array_keys($data), static::TABLE_NAME);
        $newData = SecurityService::prepareDataForQuery($data, static::TABLE_NAME);

        $id = $addEntity['class']::{'addEntity'}($newData, $fields[self::FIELDS], $fields[self::VALUES]);

        if ($id === null) {
            return false;
        }
        $data[AbstractEntity::ID] = $id;
        $baseCacheService->setCacheEntitiesById($data);

        return $id;
    }

    public function updateRecordInTable(array $data, BaseCacheService $baseCacheService): ?int
    {
        $updateEntity = $this->handleDataByMethod('update');
        $fields = $this->handlerDataService->handleFieldsForUpdate(array_keys($data), static::TABLE_NAME);
        $newData = SecurityService::prepareDataForQuery($data, static::TABLE_NAME);

        $id = $updateEntity['class']::{'updateEntity'}($newData, $fields);

        if ($id !== null) {
            $baseCacheService->updateCacheEntity($data);
            return $id;
        }

        return null;
    }

    protected function handleDataByMethod(string $method)
    {
        $method .= $this->handlerDataService->handlerClassName(static::TABLE_NAME);
        $class = $this->handlerDataService->getResourceByTableName(static::TABLE_NAME);

        return  ['class' => $class, 'method' => $method];
    }
}
