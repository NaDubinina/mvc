<?php

namespace App\Core;

use App\Core\Models\Environment;
use PDO;

class Database
{
    protected static $connection;

    public static function getConnection(): PDO
    {
        if (self::$connection) {
            return self::$connection;
        }

        $dbSettings = Environment::getFieldsStatically();

        $host = $dbSettings[Environment::HOST];
        $db   = $dbSettings[Environment::DATABASE_NAME];
        $user = $dbSettings[Environment::DATABASE_USER];
        $pass = $dbSettings[Environment::DATABASE_PASSWORD];
        $charset = $dbSettings[Environment::DATABASE_CHARSET];
        $port = $dbSettings[Environment::PORT];

        $dsn = "mysql:host=$host;port=$port;dbname=$db;charset=$charset";

        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];

        self::$connection = new PDO($dsn, $user, $pass, $opt);
        return self::$connection;
    }
}
