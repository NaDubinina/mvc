<?php

declare(strict_types=1);

namespace App\Core\Controllers\AbstractControllers;

use App\Core\Blocks\BlockFactory;
use App\Core\Models\Services\Cache\BaseCacheService;
use App\Core\Models\SqlQueries\AbstractTableQueries;
use Laminas\Di\Di;

abstract class AbstractControllerForm extends AbstractController
{
    protected AbstractTableQueries $resource;
    protected Di $di;
    protected BaseCacheService $cacheService;

    public function __construct(
        Di $di,
        BlockFactory $blockFactory,
        AbstractTableQueries $resource,
        BaseCacheService $cacheService
    ) {
        $this->di = $di;
        $this->resource = $resource;
        $this->cacheService = $cacheService;
        parent::__construct($blockFactory);
    }
}
