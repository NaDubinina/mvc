<?php

declare(strict_types=1);

namespace App\Core\Controllers\AbstractControllers;

use App\Core\Blocks\BlockFactory;
use App\Core\Exceptions\NotFoundException;

abstract class AbstractController
{
    protected ?BlockFactory $blockFactory;


    public function __construct(?BlockFactory $blockFactory)
    {
        $this->blockFactory = $blockFactory;
    }

    public function redirect(string $urn, int $statusCode = 302)
    {
        $uri = "Location: /$urn";
        header($uri, true, $statusCode);
    }

    public function setEntityId($id): self
    {
        if ($id == null) {
            http_response_code(404);
            throw new NotFoundException();
        }

        $this->id = $id;
        return $this;
    }

    protected function isGet(): bool
    {
        return $_SERVER['REQUEST_METHOD'] === 'GET' ;
    }

    protected function isPost(): bool
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

    protected function isPut(): bool
    {
        return $_SERVER['REQUEST_METHOD'] === 'PUT';
    }

    protected function isDelete(): bool
    {
        return $_SERVER['REQUEST_METHOD'] === 'DELETE';
    }

    protected function getQueryParams(): ?array
    {
        return $_GET ?? [];
    }


    public function getRequestData($entityName): ?array
    {
        $data = file_get_contents('php://input');
        $data = json_decode($data, true);

        return $data;
    }
}
