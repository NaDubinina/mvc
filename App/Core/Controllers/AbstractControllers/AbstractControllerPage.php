<?php

namespace App\Core\Controllers\AbstractControllers;

abstract class AbstractControllerPage extends AbstractController
{
    public function render()
    {
        $this->blockFactory
            ->createBlock(static::PAGE)
            ->render();
    }
}
