<?php

namespace App\Core\Controllers\Web;

use App\Core\Controllers\AbstractControllers\AbstractControllerPage;

class NotFoundControllerPage extends AbstractControllerPage
{
    public const PAGE = 'Pages\\NotFoundPage';
}
