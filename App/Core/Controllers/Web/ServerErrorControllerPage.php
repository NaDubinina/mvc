<?php

namespace App\Core\Controllers\Web;

use App\Core\Controllers\AbstractControllers\AbstractControllerPage;

class ServerErrorControllerPage extends AbstractControllerPage
{
    public const PAGE = 'Pages\\ServerErrorPage';
}
