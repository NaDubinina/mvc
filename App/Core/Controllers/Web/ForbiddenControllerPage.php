<?php

namespace App\Core\Controllers\Web;

use App\Core\Controllers\AbstractControllers\AbstractControllerPage;

class ForbiddenControllerPage extends AbstractControllerPage
{
    public const PAGE = 'Pages\\ForbiddenPage';
}
