<?php

namespace App\Core\Controllers\Api;

use App\Core\Controllers\AbstractControllers\AbstractController;
use App\Core\Models\Entities\DatabaseInformation\AbstractEntity;
use App\Core\Models\Services\Cache\BaseCacheService;
use App\Core\Models\Services\Cache\CacheServiceInterface;
use App\Core\Models\SqlQueries\AbstractTableQueries;
use Laminas\Di\Di;

abstract class AbstractControllerApi extends AbstractController
{
    private AbstractTableQueries $resource;
    private CacheServiceInterface $cacheService;
    private ?BaseCacheService $baseCacheService;

    public function __construct(Di $di, AbstractTableQueries $resource)
    {
        header('Content-type: application/json');
        $this->resource = $resource;

        $cacheService = $di->get(BaseCacheService::class, ['cacheLocation' => ($resource::TABLE_NAME . 'Cache')]);
        $this->baseCacheService = $cacheService;

        $this->cacheService = $this->baseCacheService->getCacheClass();

        parent::__construct(null);
    }

    public function convertToEntityArray(array $data): array
    {
        foreach ($data as &$entity) {
            $entity = $entity->entityToArray();
        }

        return $data;
    }

    public function isCacheData(): bool
    {
        $cache = $this->cacheService->getCache();
        if (is_null($cache)) {
            return false;
        }

        $id = $this->getQueryParams()[AbstractEntity::ID] ?? null;
        return isset($id) ? $this->baseCacheService->isCacheEntitiesById($id) : $cache[BaseCacheService::IS_CACHED_ALL];
    }

    protected function handleRequest(string $entityName)
    {
        $requestData = $this->getRequestData($entityName);
        if ($this->isGet()) {
            if ($this->isCacheData() === false) {
                $this->cacheQueryData();
            }
        } elseif ($this->isPut()) {
            $this->resource->updateRecordInTable($requestData, $this->baseCacheService);
        } elseif ($this->isPost()) {
            $this->resource->addRecordInTable($requestData, $this->baseCacheService);
        } elseif ($this->isDelete()) {
            $this->resource->deleteEntity($requestData[AbstractEntity::ID] ?? null, $this->baseCacheService);
        }
    }

    protected function cacheQueryData()
    {
        $id = $this->getQueryParams()[AbstractEntity::ID] ?? null;
        if ($id) {
            $entityById = $this->resource->getTableRecord((int)$id);
            $data[BaseCacheService::VALUES] = [$entityById->entityToArray()];
            $data[BaseCacheService::IS_CACHED_ALL] = false;
            $this->baseCacheService->setCacheEntitiesById($data);
        } else {
            $data[BaseCacheService::VALUES] = $this->resource->getTableAllRecords();
            $data[BaseCacheService::VALUES] = $this->convertToEntityArray($data[BaseCacheService::VALUES]);
            $data[BaseCacheService::IS_CACHED_ALL] = true;
            $this->cacheService->setCache($data);
        }
    }
}
