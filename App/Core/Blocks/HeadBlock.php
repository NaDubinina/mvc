<?php

namespace App\Core\Blocks;

class HeadBlock extends AbstractBlock
{
    protected ?string $block = 'head.phtml';

    private $title = null;
    private $styles = null;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title): self
    {
        $this->title = $title;
        return $this;
    }

    public function getStyles()
    {
        return $this->styles;
    }

    public function setStyles($styles): self
    {
        $this->styles = $styles;
        return $this;
    }
}
