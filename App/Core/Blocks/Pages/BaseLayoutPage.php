<?php

namespace App\Core\Blocks\Pages;

use App\Core\Blocks\BlockFactory;
use App\Core\Blocks\HeadBlock;
use App\Core\Models\SqlQueries\AbstractTableQueries;
use Laminas\Di\Di;

class BaseLayoutPage extends BaseLayout
{
    protected ?string $layout =  'main-page.phtml';
    protected ?string $title =  'Главная страница';
    protected ?string $styles =  '/assets/css/pages-styles/main-page-styles.css';
    protected $listBlock = 'Core\\Blocks\\ListBlock';
    protected $linkBlock = 'Core\\Blocks\\LinkBlock';
    protected Di $di;

    public function __construct(
        Di $di,
        AbstractTableQueries $resource,
        BlockFactory $blockFactory,
        HeadBlock $headBlock
    ) {
        $this->di = $di;
        parent::__construct($resource, $blockFactory, $headBlock);
    }

    public static function renderTemplate(string $template = null): string
    {
        if ($template) {
            ob_start();
            require APP_ROOT . $template;
            return ob_get_clean();
        }

        return '';
    }

    public function addNameEntityInTitle($name): self
    {
        $this->title .= ' ' . $name;
        return $this;
    }

    public function renderListEntity($entity)
    {
        $entityLink = str_replace('-', '_', $entity);

        ($this->blockFactory->createBlock($this->listBlock))
            ->setData($this->resource->getTableAllRecords($entity))
            ->setPageLink('/' . $entityLink)
            ->render();
    }

    public function renderSelectedDataList($receivedData)
    {
        $class = $this->methods[$receivedData]['class'];
        $resource = $this->di->newInstance($class);
        $method = $this->methods[$receivedData]['method'];
        $entity = $this->methods[$receivedData]['entity'];
        $getFieldEntity = $this->methods[$receivedData]['getFieldEntity'];
        $entityLink = $this->methods[$receivedData]['linkCard'];
        $data = $resource->{$method}($this->{$entity}->{$getFieldEntity}());

        ($this->blockFactory->createBlock($this->listBlock))
            ->setData($data)
            ->setPageLink('/' . $entityLink)
            ->render();
    }
}
