<?php

namespace App\Core\Blocks\Pages;

class NotFoundPage extends BaseLayoutPage
{
    protected ?string $layout = 'not-found-page.phtml';
    protected ?string $title =  '404';
    protected ?string $styles = '/assets/css/pages-styles/error-page-styles.css';
}
