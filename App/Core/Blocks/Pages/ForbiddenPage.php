<?php

namespace App\Core\Blocks\Pages;

class ForbiddenPage extends BaseLayoutPage
{
    protected ?string $layout = 'forbidden-page.phtml';
    protected ?string $title =  '403';
    protected ?string $styles = '/assets/css/pages-styles/error-page-styles.css';
}
