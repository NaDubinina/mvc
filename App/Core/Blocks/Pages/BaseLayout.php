<?php

namespace App\Core\Blocks\Pages;

use App\Core\Blocks\AbstractBlock;
use App\Core\Blocks\BlockFactory;
use App\Core\Blocks\HeadBlock;
use App\Core\Models\Services\HandlerDataService;
use App\Core\Models\Session;
use App\Core\Models\SqlQueries\AbstractTableQueries;

class BaseLayout extends AbstractBlock
{
    protected AbstractTableQueries $resource;
    protected BlockFactory $blockFactory;
    protected HeadBlock $headBlock;

    public function __construct(
        AbstractTableQueries $resource,
        BlockFactory $blockFactory,
        HeadBlock $headBlock
    ) {
        $this->resource = $resource;
        $this->blockFactory = $blockFactory;
        $this->headBlock  = $headBlock;
    }

    public function render()
    {
        require APP_ROOT . '/views/html/pages/' . $this->layout;
    }

    public function renderSeparator()
    {
        include APP_ROOT . '/views/html/information-separator.phtml';
    }

    public function getEntity($entity)
    {
        $entityName = HandlerDataService::handlerClassName($entity);

        $method = 'get' . ucfirst($entityName) . 'Id';
        $entityName = lcfirst($entityName);

        $this->{$entityName} = $this->resource->getTableRecord($this->{$method}(), $entity);
        return $this->{$entityName};
    }

    public function renderHeader()
    {
        include APP_ROOT . '/views/html/header.phtml';
        $this->renderSignHeader();
    }

    public function renderHead()
    {
        $this->headBlock
            ->setTitle($this->title)
            ->setStyles($this->styles)
            ->render();
    }

    public function renderSignHeader()
    {
        $isAuth = Session::isLogin();
        if ($isAuth) {
            include APP_ROOT . '/views/html/header-user-page.phtml';
        } else {
            include APP_ROOT . '/views/html/header-sign.phtml';
        }
    }

    public function renderToken()
    {
        require APP_ROOT . '/views/html/token.phtml';
    }

    public function getToken()
    {
        return Session::getToken();
    }
}
