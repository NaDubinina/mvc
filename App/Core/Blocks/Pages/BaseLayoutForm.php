<?php

namespace App\Core\Blocks\Pages;

use App\Core\Blocks\BlockFactory;
use App\Core\Blocks\HeadBlock;
use App\Core\Models\SqlQueries\AbstractTableQueries;

class BaseLayoutForm extends BaseLayout
{
    protected ?string $layout =  null;
    protected ?string $title =  null;
    protected ?string $styles =  '/assets/css/pages-styles/add-form-styles.css';
    protected $dataListBlock = 'Core\\Blocks\\DataListBlock';

    public function __construct(
        AbstractTableQueries $resource,
        BlockFactory $blockFactory,
        HeadBlock $headBlock
    ) {
        parent::__construct($resource, $blockFactory, $headBlock);
    }

    public function renderDataList($receivedData)
    {
        $class = $this->methods[$receivedData]['class'];
        $method =  $this->methods[$receivedData]['method'];

        ($this->blockFactory->createBlock($this->dataListBlock))
            ->setData($class::{$method}())
            ->render();
    }
}
