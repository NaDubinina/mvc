<?php

namespace App\Core\Blocks;

use Laminas\Di\Di;

class BlockFactory
{
    protected Di $di;

    public function __construct(Di $di)
    {
        $this->di = $di;
    }

    public function createBlock(string $blockName): AbstractBlock
    {
        $blockPath = 'App\\' . $blockName;

        /** @var $block AbstractBlock */
        $block = $this->di->newInstance($blockPath);

        return  $block;
    }
}
