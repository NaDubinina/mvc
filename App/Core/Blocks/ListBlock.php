<?php

namespace App\Core\Blocks;

class ListBlock extends AbstractBlock
{
    protected ?string $block = 'base-list.phtml';
}
