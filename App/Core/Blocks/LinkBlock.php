<?php

namespace App\Core\Blocks;

class LinkBlock extends AbstractBlock
{
    protected ?string $block = 'base-link.phtml';
}
