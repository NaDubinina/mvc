<?php

namespace App\Core\Blocks;

class DataListBlock extends AbstractBlock
{
    protected ?string $block = 'base-datalist.phtml';
}
