<?php

namespace App\Core\Routers\Web;

use App\Core\Blocks\Pages\ForbiddenPage;
use App\Core\Blocks\Pages\NotFoundPage;
use App\Core\Blocks\Pages\ServerErrorPage;
use App\Core\Exceptions\ForbiddenException;
use App\Core\Exceptions\NotFoundException;
use App\Core\Exceptions\ServerErrorException;
use App\Core\Models\Services\Logger\Logger;
use Laminas\Di\Di;

class RouterPool
{
    private array $routers;
    private Logger $logger;
    private Di $di;

    public function __construct(Logger $logger, DI $di, array $routers = [])
    {
        $this->routers = $routers;
        $this->logger = $logger;
        $this->di = $di;
    }

    public function goToPage($requestUri)
    {
        try {
            foreach ($this->routers as $routerClass) {
                if ($routerClass->goToPage($requestUri)) {
                    break;
                }
            }
        } catch (NotFoundException $e) {
            $this->logger->writeLogError($e->getMessage());
            ($this->di->get(NotFoundPage::class))->render();
        } catch (ServerErrorException $e) {
            $this->logger->writeLogError($e->getMessage());
            ($this->di->get(ServerErrorPage::class))->render();
        } catch (ForbiddenException $e) {
            $this->logger->writeLogError($e->getMessage());
            ($this->di->get(ForbiddenPage::class))->render();
        }
    }
}
