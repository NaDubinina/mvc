<?php

namespace App\Core\Routers\Web;

use App\Core\Exceptions\NotFoundException;
use App\ModuleSettingsAggregator;
use Laminas\Di\Di;

class ApiRouter extends AbstractRouter
{
    private Di $di;
    protected ?array $routes = null;

    public function __construct(Di $di)
    {
        $this->di = $di;
    }

    public function goToPage(string $requestUri): bool
    {
        $controller = null;
        $this->routes = ModuleSettingsAggregator::getApiRoutes();

        foreach ($this->routes as $key => $value) {
            $pos = stripos($requestUri, $key);
            if ($pos !== false) {
                $controller = $value;
                $controller = $this->di->get($controller);
            }
        }

        if (!$controller && (stripos($requestUri, '/api/') === true)) {
            http_response_code(404);
            throw new NotFoundException('Неизвестная страница по адресу ' . $requestUri);
        }
        if ($controller) {
            $controller->execute();
            return true;
        }
        return false;
    }
}
