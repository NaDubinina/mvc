<?php

namespace App\Core\Routers\Web;

abstract class AbstractRouter
{
    abstract public function goToPage(string $requestUri): bool;
}
