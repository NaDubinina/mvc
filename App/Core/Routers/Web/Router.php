<?php

namespace App\Core\Routers\Web;

use App\Core\Exceptions\ForbiddenException;
use App\Core\Exceptions\NotFoundException;
use App\Core\Exceptions\ServerErrorException;
use App\Core\FileFinder;
use App\Core\Models\Session;
use Laminas\Di\Di;

class Router extends AbstractRouter
{
    public const GET = 'GET';
    public const POST = 'POST';
    private Di $di;
    private $classControl = null;
    private $method = null;
    private $entityName = null;

    public function __construct(Di $di)
    {
        $this->di = $di;
    }

    public function goToPage(string $uri): bool
    {
        $this->handlerUri($uri);

        $this->createNewController($this->classControl, $this->entityName)
            ->{$this->method}();

        return true;
    }

    public function handlerUri(string $requestUri)
    {
        Session::start();

        if (http_response_code() == 500) {
            throw new ServerErrorException();
        }

        $this->convertUriToControllerName($requestUri);

        switch ($_SERVER['REQUEST_METHOD']) {
        //switch (self::GET) {
            case (self::GET):
                $this->method = 'render';
                break;
            case (self::POST):
                if ($this->checkNeedToken($requestUri)) {
                    $this->method = 'submitForm';
                }
                break;
        }
    }

    protected function createNewController($className, $entityName)
    {
        $entityName = ($entityName != '') ? $entityName : 'Core';
        $classController = "App\\{$entityName}\\Controllers\\Web\\" . $className;
        $classController = $this->di->get($classController);

        if (isset($_GET['id'])) {
            $classController->setEntityId($_GET['id']);
        }

        return $classController;
    }

    protected function getEntityName(array $words)
    {
        $entityNameIndex = (in_array($words[0], ['add', 'update', 'delete', 'my'])) ? 1 : 0;
        $entityName = $words[$entityNameIndex];

        if (substr($entityName, -1) !== 's') {
            $this->entityName = ucfirst($entityName);
            return;
        }
        $entityName = substr($entityName, 0, -1);

        if (substr($entityName, -2) == 'ie') {
            $entityName = (substr($entityName, 0, -2) . 'y');
        }

        $this->entityName = ucfirst($entityName);
    }

    protected function convertUriToControllerName($requestUri)
    {
        if (($requestUri === '/' || $requestUri === '/index')) {
            $this->classControl = 'UsersControllerPage';
            $this->entityName = 'User';
            return;
        }

        $uri = ltrim($requestUri, '/');
        $uriSplit = explode("?", $uri);

        $pageSplit = explode("-", $uriSplit[0]);
        $pageName = null;

        $this->getEntityName($pageSplit);

        foreach ($pageSplit as $word) {
            $pageName .= ucfirst($word);
        }

        $this->classControl = $pageName . 'ControllerPage';

        $allControllers = FileFinder::findControllers($this->entityName);

        if (!in_array($this->classControl, $allControllers)) {
            http_response_code(404);
            throw new NotFoundException('Неизвестная страница по адресу ' . $requestUri);
        }
    }

    protected function checkNeedToken($requestUri): bool
    {
        if (!(in_array($this->classControl, ['UserAuthorizationControllerPage', 'UserRegistrationControllerPage']))) {
            if (Session::getToken() != $_POST['token'] || Session::getToken() == null) {
                http_response_code(403);
                throw new ForbiddenException(
                    'Попытка доступа без необходимых прав к странице ' . $requestUri
                );
            }
        }

        return true;
    }
}
