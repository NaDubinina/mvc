<?php

namespace App\Core\Routers\Console;

use App\Core\Exceptions\InvalidParametersException;
use App\Core\Exceptions\UnknownCommandException;
use App\Core\Models\Services\Logger\Logger;
use App\ModuleSettingsAggregator;
use Laminas\Di\Di;

class CliRouter
{
    public const COMMAND = 'command';
    public const CLASS_NAME = 'class';
    public const PARAMETERS = 'params';
    public const METHOD = 'method';
    private $consoleCommands = [];
    private $paramValues = [];
    private $param = [];
    private $consoleCommand;
    private string $classCommand;
    private Di $di;

    public function __construct(Di $di)
    {
        $this->di = $di;
    }

    public function execute($argument)
    {
        try {
            $this->loadCommandAndParams();
            $this->selectCommandClass($argument[self::COMMAND]);
            $this->handleExportParam($argument);

            if ($this->isSetNecessaryParameters()) {
                $commandClass = $this->di->get($this->classCommand);
                $commandClass->execute($this->param);
            }
        } catch (InvalidParametersException $e) {
            (new Logger())->writeLogError($e->getMessage());
        } catch (UnknownCommandException $e) {
            (new Logger())->writeLogError($e->getMessage());
        }
    }

    public function isSetNecessaryParameters(): bool
    {
        $necessaryParameters = $this->consoleCommands[$this->consoleCommand][self::PARAMETERS];
        $param = array_keys($this->param);
        $paramNotFound = array_diff($necessaryParameters, $param);

        if (count($paramNotFound) !== 0) {
            throw new InvalidParametersException(
                'Неверное количество параметров при использовании команды ' . $this->consoleCommand
            );
        }
        return true;
    }

    protected function selectCommandClass(string $command)
    {
        if (!array_key_exists($command, $this->consoleCommands)) {
            throw new UnknownCommandException('Команда ' . $command . ' не найдена.');
        }
        $this->consoleCommand = $command;
        $this->classCommand = $this->consoleCommands[$command][self::CLASS_NAME];
    }

    protected function handleExportParam(?array $arguments)
    {
        $param = '/--(.*?)=(.*)/';
        foreach ($arguments as $argument) {
            if (preg_match($param, $argument, $matches)) {
                $paramName = $matches[1] ?? null;
                $paramValue = $matches[2] ?? null;
                $this->param[$paramName] = $this->paramValues[$paramValue] ?? $paramValue;
            }
        }
    }

    protected function loadCommandAndParams()
    {
        $this->consoleCommands = ModuleSettingsAggregator::getCliCommandRoutes();
        $this->paramValues = ModuleSettingsAggregator::getCliParamsRoutes();
    }
}
