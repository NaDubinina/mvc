<?php

namespace App\Core;

class FileFinder
{
    public static function findControllers($entityName): array
    {
        $filesAll = scandir(APP_ROOT . "/App/{$entityName}/Controllers/Web");
        $filesController = array_filter($filesAll, function ($fileName) {
            return stripos($fileName, '.php') !== false;
        });

        return   str_replace('.php', '', $filesController);
    }
}
