<?php

namespace App\Bookcase\Models\SqlQueries;

use App\Bookcase\Models\Entity\Bookcase;
use App\Core\Database;
use App\Core\Models\SqlQueries\AbstractTableQueries;

class TableBookcaseQueries extends AbstractTableQueries
{
    public const TABLE_NAME = 'bookcase';
    public static function getFields(): array
    {
        return [
            'bookcase' => [
                'name',
                'number_rows',
                'library_id'
            ]
        ];
    }

    public function getBookcase($id): Bookcase
    {
        $connection = Database::getConnection();

        $query = $connection->prepare('select * from bookcase where id = ?');
        $query->execute([$id]);
        return $this->entitiesHandler->handleBookcase($query->fetch(), $this->entitiesHandler);
    }

    public function getLibraryBookcase($libraryId): array
    {
        $connection = Database::getConnection();

        $query = $connection->prepare('select * from bookcase where library_id = ? ');
        $query->execute([$libraryId]);
        $bookcases = $this->entitiesHandler->handleEntities($query->fetchAll(), 'bookcase');

        return $bookcases;
    }

    public function getLocation(): array
    {
        $connection = Database::getConnection();
        $query = 'select library.address, bookcase.name, bookcase.id from library 
                    inner join bookcase where library.id = bookcase.library_id';
        $query = $connection->query($query);
        $locations = [];
        $queryData = $query->fetchAll();

        foreach ($queryData as $location) {
            $locations[] = [
                'id'   => $location['id'],
                'name' => $location['address'] . ' ' . $location['name'],
            ];
        }

        return $locations;
    }
}
