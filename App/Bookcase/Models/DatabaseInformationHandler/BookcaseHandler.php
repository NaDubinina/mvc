<?php

namespace App\Bookcase\Models\DatabaseInformationHandler;

use App\Bookcase\Models\Entity\Bookcase;
use App\Core\Models\Entities\DatabaseInformationHandler\EntitiesHandler;

class BookcaseHandler extends EntitiesHandler
{
    public function handleBookcase($bookcaseDB): Bookcase
    {
        $bookcaseDB = $this->replaceThreateningSymbols($bookcaseDB);
        return ($this->di->newInstance(Bookcase::class))
            ->setId($bookcaseDB['id'])
            ->setName($bookcaseDB['name'])
            ->setLibraryId($bookcaseDB['library_id'])
            ->setNumberRows($bookcaseDB['number_rows']);
    }
}
