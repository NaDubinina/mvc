<?php

namespace App\Bookcase\Blocks;

use App\Core\Blocks\Pages\BaseLayoutForm;

class AddBookcasePage extends BaseLayoutForm
{
    protected ?string $layout = 'add-bookcase-page.phtml';
    protected ?string $title = 'Добавление книжного стелажа';
    private $libraryId = null;

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getLibraryId()
    {
        return $this->libraryId;
    }

    public function setLibraryId($libraryId): self
    {
        $this->libraryId = $libraryId;
        return $this;
    }
}
