<?php

namespace App\Bookcase\Blocks;

use App\Book\Models\SqlQueries\TableBookCopyQueries;
use App\Bookcase\Models\Entity\Bookcase;
use App\Core\Blocks\Pages\BaseLayoutPage;

class BookcasePage extends BaseLayoutPage
{
    protected ?string $layout = 'bookcase-page.phtml';
    protected ?string $title =  'Книжный шкаф';
    protected ?string $styles = '/assets/css/pages-styles/bookcase-page-styles.css';
    protected ?Bookcase $bookcase = null;
    protected $methods = [
        'all the books from the bookcase' => [
            'class' => TableBookCopyQueries::class,
            'method' => 'getBookBookcase',
            'entity' => 'bookcase',
            'linkCard' => 'book-copy',
            'getFieldEntity' => 'getId',
        ],
    ];
    private $id = null;

    public function getBookcaseId(): int
    {
        return $this->id;
    }

    public function setBookcaseId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function renderListBookcaseBooks()
    {
        $this->renderSelectedDataList('all the books from the bookcase');
    }
}
