<?php

namespace App\Bookcase\Blocks;

use App\Core\Blocks\Pages\BaseLayoutForm;
use App\Library\Models\SqlQueries\TableLibraryQueries;

class UpdateBookcasePage extends BaseLayoutForm
{
    protected ?string $layout = 'update-bookcase-page.phtml';
    protected ?string $title = 'Изменение книжного шкафа';
    protected $methods = [
        'list of possible library' => [
            'class' => TableLibraryQueries::class,
            'method' => 'getLibrary'
        ],
    ];

    private $bookcaseId = null;

    public function getBookcaseId(): int
    {
        return $this->bookcaseId;
    }

    public function setBookcaseId($bookcaseId): self
    {
        $this->bookcaseId = $bookcaseId;
        return $this;
    }

    public function renderListLibrary()
    {
        $this->renderDataList('list of possible library');
    }
}
