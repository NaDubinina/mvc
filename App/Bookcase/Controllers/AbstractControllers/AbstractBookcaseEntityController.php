<?php

namespace App\Bookcase\Controllers\AbstractControllers;

use App\Bookcase\Models\SqlQueries\TableBookcaseQueries;
use App\Core\Blocks\BlockFactory;
use App\Core\Controllers\AbstractControllers\AbstractControllerForm;
use App\Core\Models\Services\Cache\BaseCacheService;
use Laminas\Di\Di;

class AbstractBookcaseEntityController extends AbstractControllerForm
{
    private const CACHE_NAME = 'BookcaseCache';
    public const PAGE = '';

    public function __construct(
        Di $di,
        BlockFactory $blockFactory,
        TableBookcaseQueries $resource,
        BaseCacheService $cacheService
    ) {
        $cacheService->getCacheClass()->setCacheName(self::CACHE_NAME);
        parent::__construct($di, $blockFactory, $resource, $cacheService);
    }

    public function render()
    {
        $this->blockFactory
            ->createBlock(static::PAGE)
            ->setBookcaseId($_GET['id'])
            ->render();
    }
}
