<?php

namespace App\Bookcase\Controllers\Web;

use App\Core\Controllers\AbstractControllers\AbstractControllerPage;

class BookcaseControllerPage extends AbstractControllerPage
{
    protected $id = null;
    public const PAGE = 'Bookcase\\Blocks\\BookcasePage';

    public function render()
    {
        $this->blockFactory
            ->createBlock(self::PAGE)
            ->setBookcaseId($this->id)
            ->render();
    }
}
