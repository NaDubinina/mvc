<?php

namespace App\Bookcase\Controllers\Web;

use App\Bookcase\Controllers\AbstractControllers\AbstractBookcaseEntityController;
use App\Core\Models\Services\SecurityService;

class DeleteBookcaseControllerPage extends AbstractBookcaseEntityController
{
    public const PAGE = 'Bookcase\\Blocks\\DeleteBookcasePage';

    public function submitForm()
    {
        $prepareFormData = SecurityService::prepareDataForQuery($_POST, 'bookcase');
        $this->resource->deleteRecord($prepareFormData[':id']);
        $this->redirect('library?id=' . $prepareFormData[':library_id']);
    }
}
