<?php

namespace App\Bookcase\Controllers\Web;

use App\Bookcase\Controllers\AbstractControllers\AbstractBookcaseEntityController;
use App\Core\Models\Services\HandlerDataService;

class UpdateBookcaseControllerPage extends AbstractBookcaseEntityController
{
    public const PAGE = 'Bookcase\\Blocks\\UpdateBookcasePage';

    public function submitForm()
    {
        $data = HandlerDataService::handlerSubmitData($_POST);
        $this->redirect('bookcase?id=' . $this->resource->updateRecordInTable($data, $this->cacheService));
    }
}
