<?php

namespace App\Bookcase\Controllers\Web;

use App\Bookcase\Controllers\AbstractControllers\AbstractBookcaseEntityController;
use App\Core\Models\Services\HandlerDataService;

class AddBookcaseControllerPage extends AbstractBookcaseEntityController
{
    public const PAGE = 'Bookcase\\Blocks\\AddBookcasePage';

    public function render()
    {
        $this->blockFactory
            ->createBlock(self::PAGE)
            ->setLibraryId($_GET['library-id'])
            ->render();
    }

    public function submitForm()
    {
        $data = HandlerDataService::handlerSubmitData($_POST);
        $this->redirect('bookcase?id=' . $this->resource->addRecordInTable($data, $this->cacheService));
    }
}
