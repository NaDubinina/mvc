<?php

declare(strict_types=1);

namespace App;

class ModuleSettingsAggregator
{
    public const WEB_ROUTES_SETTINGS = 'webRoutes';
    public const CLI_COMMAND_ROUTES_SETTINGS = 'cliCommandRoutes';
    public const CLI_PARAMS_ROUTES_SETTINGS = 'cliParamsRoutes';
    public const API_ROUTES_SETTINGS = 'apiRoutes';

    protected static ?array $mergedSettings = null;
    protected static array $routesSettings = [
        self::WEB_ROUTES_SETTINGS,
        self::CLI_COMMAND_ROUTES_SETTINGS,
        self::CLI_PARAMS_ROUTES_SETTINGS,
        self::API_ROUTES_SETTINGS,
    ];

    public static function getWebRoutes(): array
    {
        return self::getMergedModuleSettings()[self::WEB_ROUTES_SETTINGS] ?? [];
    }

    public static function getCliCommandRoutes(): array
    {
        return self::getMergedModuleSettings()[self::CLI_COMMAND_ROUTES_SETTINGS] ?? [];
    }

    public static function getCliParamsRoutes(): array
    {
        return self::getMergedModuleSettings()[self::CLI_PARAMS_ROUTES_SETTINGS] ?? [];
    }


    public static function getApiRoutes(): array
    {
        return self::getMergedModuleSettings()[self::API_ROUTES_SETTINGS] ?? [];
    }

    public static function getModules(): array
    {
        return include APP_ROOT . '/App/modules.php';
    }

    public static function getModuleDir($entityName, $separator = '_'): string
    {
        $entitySplit = explode($separator, $entityName);
        $modules = self::getModules();
        foreach ($entitySplit as $item) {
            if (in_array($item, $modules)) {
                return ucfirst($item);
            }
        }
        return '';
    }

    protected static function getMergedModuleSettings(): array
    {
        if (self::$mergedSettings !== null) {
            return self::$mergedSettings;
        }

        self::$mergedSettings = [];

        $modules = self::getModules();

        foreach ($modules as $module) {
            $settings = include_once  APP_ROOT . '/App/' . ucfirst($module) . '/settings.php';

            foreach (self::$routesSettings as $section) {
                if (!isset(self::$mergedSettings[$section])) {
                    self::$mergedSettings[$section] = [];
                }

                $settingsSection = $settings[$section] ?? [];
                self::$mergedSettings[$section] += $settingsSection;
            }
        }

        return self::$mergedSettings;
    }
}
