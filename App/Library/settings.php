<?php

use App\Library\Controllers\Api\LibrariesControllerApi;
use App\ModuleSettingsAggregator;

return [
    ModuleSettingsAggregator::WEB_ROUTES_SETTINGS => [],
    ModuleSettingsAggregator::API_ROUTES_SETTINGS => [
        '/api/libraries'  => LibrariesControllerApi::class
    ],
    ModuleSettingsAggregator::CLI_COMMAND_ROUTES_SETTINGS => [],
    ModuleSettingsAggregator::CLI_PARAMS_ROUTES_SETTINGS => []
];
