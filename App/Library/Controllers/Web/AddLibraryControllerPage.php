<?php

namespace App\Library\Controllers\Web;

use App\Core\Models\Services\HandlerDataService;
use App\Library\Controllers\AbstractControllers\AbstractLibraryEntityController;

class AddLibraryControllerPage extends AbstractLibraryEntityController
{
    public const PAGE = 'Library\\Blocks\\AddLibraryPage';

    public function render()
    {
        $this->blockFactory
            ->createBlock(self::PAGE)
            ->render();
    }

    public function submitForm()
    {
        $data = HandlerDataService::handlerSubmitData($_POST);
        $this->redirect('library?id=' . $this->resource->addRecordInTable($data, $this->cacheService));
    }
}
