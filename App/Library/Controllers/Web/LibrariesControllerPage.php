<?php

namespace App\Library\Controllers\Web;

use App\Core\Controllers\AbstractControllers\AbstractControllerPage;

class LibrariesControllerPage extends AbstractControllerPage
{
    public const PAGE = 'Library\\Blocks\\LibrariesPage';
}
