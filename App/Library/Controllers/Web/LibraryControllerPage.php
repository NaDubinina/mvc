<?php

namespace App\Library\Controllers\Web;

use App\Core\Controllers\AbstractControllers\AbstractControllerPage;

class LibraryControllerPage extends AbstractControllerPage
{
    protected $id = null;
    public const PAGE = 'Library\\Blocks\\LibraryPage';

    public function render()
    {
        $this->blockFactory
            ->createBlock(self::PAGE)
            ->setLibraryId($this->id)
            ->render();
    }
}
