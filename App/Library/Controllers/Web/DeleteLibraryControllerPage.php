<?php

namespace App\Library\Controllers\Web;

use App\Library\Controllers\AbstractControllers\AbstractLibraryEntityController;
use App\Core\Models\Services\SecurityService;

class DeleteLibraryControllerPage extends AbstractLibraryEntityController
{
    public const PAGE = 'Library\\Blocks\\DeleteLibraryPage';

    public function submitForm()
    {
        $prepareFormData = SecurityService::prepareDataForQuery($_POST, 'library');
        $this->resource->deleteRecord($prepareFormData[':id']);
        $this->redirect('libraries');
    }
}
