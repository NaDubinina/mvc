<?php

namespace App\Library\Controllers\Web;

use App\Core\Models\Services\HandlerDataService;
use App\Library\Controllers\AbstractControllers\AbstractLibraryEntityController;

class UpdateLibraryControllerPage extends AbstractLibraryEntityController
{
    public const PAGE = 'Library\\Blocks\\UpdateLibraryPage';

    public function submitForm()
    {
        $data = HandlerDataService::handlerSubmitData($_POST);
        $this->redirect('library?id=' . $this->resource->updateRecordInTable($data, $this->cacheService));
    }
}
