<?php

namespace App\Library\Controllers\Api;

use App\Core\Controllers\Api\AbstractControllerApi;
use App\Library\Models\SqlQueries\TableLibraryQueries;
use Laminas\Di\Di;

class LibrariesControllerApi extends AbstractControllerApi
{
    private $tableName = 'library';

    public function __construct(TableLibraryQueries $resource, Di $di)
    {
        parent::__construct($di, $resource);
    }

    public function execute()
    {
        $this->handleRequest($this->tableName);
    }
}
