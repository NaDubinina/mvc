<?php

namespace App\Library\Controllers\AbstractControllers;

use App\Core\Blocks\BlockFactory;
use App\Core\Controllers\AbstractControllers\AbstractControllerForm;
use App\Core\Models\Services\Cache\BaseCacheService;
use App\Library\Models\SqlQueries\TableLibraryQueries;
use Laminas\Di\Di;

class AbstractLibraryEntityController extends AbstractControllerForm
{
    private const CACHE_NAME = 'LibraryCache';

    public function __construct(
        Di $di,
        BlockFactory $blockFactory,
        TableLibraryQueries $resource,
        BaseCacheService $cacheService
    ) {
        $cacheService->getCacheClass()->setCacheName(self::CACHE_NAME);
        parent::__construct($di, $blockFactory, $resource, $cacheService);
    }

    public function render()
    {
        $this->blockFactory
            ->createBlock(static::PAGE)
            ->setLibraryId($_GET['id'])
            ->render();
    }
}
