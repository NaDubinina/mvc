<?php

namespace App\Library\Models\SqlQueries;

use App\Core\Database;
use App\Core\Models\SqlQueries\AbstractTableQueries;

class TableLibraryQueries extends AbstractTableQueries
{
    public const TABLE_NAME = 'library';

    public static function getFields(): array
    {
        return [
            'library' => [
                'name',
                'address',
                'founding_date'
            ]
        ];
    }

    public function setNumberOfBooks($libraryId): int
    {
        $connection = Database::getConnection();
        $query = 'select count(book_copy.id ) as all_books_in_library  from  publisher 
            inner join book on publisher.id = book.publisher_id 
            inner join book_copy on book.id = book_copy.book_id 
            inner join bookcase on book_copy.bookcase_id = bookcase.id 
            right join library on library.id = bookcase.library_id 
            where library.id = ?';
        $query = $connection->prepare($query);
        $query->execute([$libraryId]);
        $numberOfBook = $query->fetch();
        return $numberOfBook['all_books_in_library'];
    }

    public function getLibrary(): array
    {
        $connection = Database::getConnection();
        $query = 'select id, name from library';
        $query = $connection->query($query);
        $locations = [];
        $queryData = $query->fetchAll();

        foreach ($queryData as $location) {
            $locations[] = [
                'id'   => $location['id'],
                'name' => $location['name'],
            ];
        }

        return $locations;
    }
}
