<?php

namespace App\Library\Models\DatabaseInformationHandler;

use App\Core\Models\Entities\DatabaseInformationHandler\EntitiesHandler;
use App\Library\Models\Entity\Library;

class LibraryHandler extends EntitiesHandler
{
    public function handleLibrary($libraryDB): Library
    {
        $libraryDB = $this->replaceThreateningSymbols($libraryDB);
        return ($this->di->newInstance(Library::class))
            ->setId($libraryDB['id'])
            ->setName($libraryDB['name'])
            ->setFoundingDate($libraryDB['founding_date'])
            ->setAddress($libraryDB['address']);
    }
}
