<?php

namespace App\Library\Blocks;

use App\Bookcase\Models\SqlQueries\TableBookcaseQueries;
use App\Core\Blocks\BlockFactory;
use App\Core\Blocks\HeadBlock;
use App\Core\Blocks\Pages\BaseLayoutPage;
use App\Library\Models\SqlQueries\TableLibraryQueries;
use Laminas\Di\Di;

class LibraryPage extends BaseLayoutPage
{
    protected ?string $layout = 'library-page.phtml';
    protected ?string $title =  'Библиотека';
    protected ?string $styles = '/assets/css/pages-styles/library-page-styles.css';
    protected $methods = [
        'bookcases located in the library' => [
            'class' => TableBookcaseQueries::class,
            'method' => 'getLibraryBookcase',
            'entity' => 'library',
            'linkCard' => 'bookcase',
            'getFieldEntity' => 'getId',
        ],
    ];
    private $id = null;

    public function __construct(Di $di, TableLibraryQueries $resource, BlockFactory $blockFactory, HeadBlock $headBlock)
    {
        parent::__construct($di, $resource, $blockFactory, $headBlock);
    }

    public function getLibraryId(): int
    {
        return $this->id;
    }

    public function setLibraryId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getNumberOfBooks($libraryData): int
    {
        return $this->resource->setNumberOfBooks($libraryData->getId());
    }

    public function renderListLibraryBookcases()
    {
        $this->renderSelectedDataList('bookcases located in the library');
    }
}
