<?php

namespace App\Library\Blocks;

use App\Core\Blocks\Pages\BaseLayoutPage;

class LibrariesPage extends BaseLayoutPage
{
    protected ?string $layout = 'libraries-page.phtml';
    protected ?string $title =  'Библиотеки';
    protected ?string $styles = '/assets/css/pages-styles/libraries-page-styles.css';
}
