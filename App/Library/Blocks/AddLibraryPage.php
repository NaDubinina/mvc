<?php

namespace App\Library\Blocks;

use App\Core\Blocks\Pages\BaseLayoutForm;

class AddLibraryPage extends BaseLayoutForm
{
    protected ?string $layout = 'add-library-page.phtml';
    protected ?string $title = 'Добавление библиотеки';
}
