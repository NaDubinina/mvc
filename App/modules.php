<?php

return [
    'core',
    'book',
    'bookcase',
    'category',
    'country',
    'library',
    'publisher',
    'user',
    'cron',
];
