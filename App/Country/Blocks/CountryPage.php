<?php

namespace App\Country\Blocks;

use App\Book\Models\SqlQueries\TableBookQueries;
use App\Core\Blocks\Pages\BaseLayoutPage;

class CountryPage extends BaseLayoutPage
{
    protected ?string $layout = 'country-page.phtml';
    protected ?string $title =  'Страна';
    protected ?string $styles = '/assets/css/pages-styles/country-page-styles.css';
    protected $methods = [
        'all books published in the country' => [
            'class' => TableBookQueries::class,
            'method' => 'getCountryBooks',
            'entity' => 'country',
            'linkCard' => 'book',
            'getFieldEntity' => 'getId',
        ],
    ];
    private $id = null;

    public function getCountryId(): int
    {
        return $this->id;
    }

    public function setCountryId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function renderListCountry()
    {
        $this->renderSelectedDataList('all books published in the country');
    }
}
