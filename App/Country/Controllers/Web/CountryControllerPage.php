<?php

namespace App\Country\Controllers\Web;

use App\Core\Controllers\AbstractControllers\AbstractControllerPage;

class CountryControllerPage extends AbstractControllerPage
{
    protected $id = null;
    public const PAGE = 'Country\Blocks\CountryPage';

    public function render()
    {
        $this->blockFactory
            ->createBlock(self::PAGE)
            ->setCountryId($this->id)
            ->render();
    }
}
