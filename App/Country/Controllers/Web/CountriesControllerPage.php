<?php

namespace App\Country\Controllers\Web;

use App\Core\Controllers\AbstractControllers\AbstractControllerPage;

class CountriesControllerPage extends AbstractControllerPage
{
    public function render()
    {
        echo 'Countries page';
    }
}
