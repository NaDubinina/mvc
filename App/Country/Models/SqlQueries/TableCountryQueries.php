<?php

namespace App\Country\Models\SqlQueries;

use App\Core\Database;
use App\Core\Models\SqlQueries\AbstractTableQueries;

class TableCountryQueries extends AbstractTableQueries
{
    public const TABLE_NAME = 'country';

    public static function getFields(): array
    {
        return [
           'country' => ['name']
        ];
    }

    public function getCountries(): array
    {
        $connection = Database::getConnection();
        $query = 'select * from country';
        $query = $connection->query($query);

        $countryList = $query->fetchAll();

        return $countryList;
    }
}
