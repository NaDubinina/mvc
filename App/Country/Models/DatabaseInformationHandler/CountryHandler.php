<?php

namespace App\Country\Models\DatabaseInformationHandler;

use App\Core\Models\Entities\DatabaseInformationHandler\EntitiesHandler;
use App\Country\Models\Entity\Country;

class CountryHandler extends EntitiesHandler
{
    public function handleCountry($countryDB): Country
    {
        $countryDB = $this->replaceThreateningSymbols($countryDB);
        return ($this->di->newInstance(Country::class))
            ->setId($countryDB['id'])
            ->setName($countryDB['name']);
    }
}
