<?php

namespace App\Country\Models\Entity;

use App\Core\Models\Entities\DatabaseInformation\AbstractEntity;

class Country extends AbstractEntity
{
    private $id = null;
    private $name = null;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }

    public function entityToArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
